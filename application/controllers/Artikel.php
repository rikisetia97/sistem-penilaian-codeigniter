<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class artikel extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

	}

	public function index()
	{
		$data = [
			'title' => 'Artikel - SINILAI'
		];
		$this->load->view('public/artikel', $data);
		
	}
}
