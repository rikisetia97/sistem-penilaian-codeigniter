<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Akademik extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if (!auth_check()) {
			redirect('auth/login');
		}
		if ($this->session->userdata('sess_role') == 'siswa') {
			redirect('.');
		}
		$this->load->model('Akademik_model', 'Akademik');
	}

	// GET JURUSAN
	public function jurusan()
	{
		$data = [
			'title' => 'Akademik ~ Jurusan',
			'jurusan' => $this->Akademik->get('jurusan'),
		];
		$this->load->view('pages/akademik/jurusan', $data);
	}

	// GET KELAS
	public function kelas()
	{
		$data = [
			'title' => 'Akademik ~ Kelas',
			'kelas' => $this->Akademik->get('kelas'),
		];
		$this->load->view('pages/akademik/kelas', $data);
	}

	// GET PELAJARAN
	public function pelajaran()
	{
		$data = [
			'title' => 'Akademik ~ Mata Pelajaran',
			'pelajaran' => $this->Akademik->get('pelajaran'),
		];
		$this->load->view('pages/akademik/pelajaran', $data);
	}

	// GET TAHUN AJARAN
	public function tahun_ajaran()
	{
		$data = [
			'title' => 'Akademik ~ Tahun Ajaran',
			'tahun_ajaran' => $this->Akademik->get('tahun_ajaran'),
		];
		$this->load->view('pages/akademik/tahun_ajaran', $data);
	}

	// INSERT JURUSAN
	public function jurusan_create()
	{
		$data = (object) $this->input->post();
		$insert = $this->Akademik->insert_jurusan($data);
		if (!$insert) {
			$this->session->set_flashdata('msg', 'failed');
			redirect($this->agent->referrer());
		}else{
			$this->session->set_flashdata('msg', 'success');
			redirect($this->agent->referrer());
		}
	}

	// INSERT KELAS
	public function kelas_create()
	{
		$data = (object) $this->input->post();
		$insert = $this->Akademik->insert_kelas($data);
		if (!$insert) {
			$this->session->set_flashdata('msg', 'failed');
			redirect($this->agent->referrer());
		}else{
			$this->session->set_flashdata('msg', 'success');
			redirect($this->agent->referrer());
		}
	}

	// INSERT PELAJARAN
	public function pelajaran_create()
	{
		$data = (object) $this->input->post();
		$insert = $this->Akademik->insert_pelajaran($data);
		if (!$insert) {
			$this->session->set_flashdata('msg', 'failed');
			redirect($this->agent->referrer());
		}else{
			$this->session->set_flashdata('msg', 'success');
			redirect($this->agent->referrer());
		}
	}

	// INSERT TAHUN AJARAN
	public function tahun_ajaran_create()
	{
		$data = (object) $this->input->post();
		$insert = $this->Akademik->insert_tahun_ajaran($data);
		if (!$insert) {
			$this->session->set_flashdata('msg', 'failed');
			redirect($this->agent->referrer());
		}else{
			$this->session->set_flashdata('msg', 'success');
			redirect($this->agent->referrer());
		}
	}

	// GET DETAIL MULTIPLE
	public function detail($id, $table)
	{
		$data = $this->Akademik->detail($id, $table);
		echo json_encode($data);
	}


	// UPDATE JURUSAN
	public function jurusan_update()
	{
		$data = $this->input->post();
		$return = $this->Akademik->update_jurusan($data);
		if ($return) {
			$this->session->set_flashdata('msg', 'update');
		}else{
			$this->session->set_flashdata('msg', 'update_fail');
		}
		redirect($this->agent->referrer());
	}

	// UPDATE PELAJARAN
	public function pelajaran_update()
	{
		$data = $this->input->post();
		$return = $this->Akademik->update_pelajaran($data);
		if ($return) {
			$this->session->set_flashdata('msg', 'update');
		}else{
			$this->session->set_flashdata('msg', 'update_fail');
		}
		redirect($this->agent->referrer());
	}

	// UPDATE TAHUN AJARAN
	public function tahun_ajaran_update()
	{
		$data = $this->input->post();
		$return = $this->Akademik->update_tahun_ajaran($data);
		if ($return) {
			$this->session->set_flashdata('msg', 'update');
		}else{
			$this->session->set_flashdata('msg', 'update_fail');
		}
		redirect($this->agent->referrer());
	}

	// DELETE MULTIPLE
	public function delete($id, $table)
	{
		$this->Akademik->delete($id, $table);
		$this->session->set_flashdata('msg', 'delete');
		redirect($this->agent->referrer());
	}

}
