<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

	}

	public function index()
	{
		if (!auth_check()) {
			$data = [
				'title' => 'Dashboard ~ SMK MUHAMKA',
			];
			$this->load->view('public/home', $data);
		}else{
			$data = [
				'title' => 'Dashboard ~ SMK MUHAMKA',
			];
			$this->load->view('pages/home', $data);
		}
	}
}
