<?php defined('BASEPATH') OR die('No direct script access allowed');

use alhimik1986\PhpExcelTemplator\PhpExcelTemplator;

class Export extends CI_Controller {

  public function __construct()
  {
    parent::__construct();
    if (!auth_check()) {
      redirect('auth/login');
    }
    $this->load->library('PDF');
    $this->load->model('Nilai_model', 'Nilai');
  }

  public function nilai($data, $format)
  {
    $data = explode("|", base64_decode($data));
    $datas = $this->Nilai->getUserNilai($data);
    if ($format == 'excel') {
      $nomor = [];
      $no_induk = [];
      $nama_siswa = [];
      $nilai_kkm = [];
      $nilai = [];
      $i=1;
      foreach ($datas['user'] as $key) {
        $nomor[] = $i;
        $no_induk[] = $this->db->get_where('users', ['id' => $key['nilai']->id_users])->row()->no_induk;
        $nama_siswa[] = $key['nama'];
        $nilai_kkm[] = $key['nilai']->kkm;
        $nilai[] = $key['nilai']->nilai;
        $i++;
      }
      header('Content-Type: application/vnd.ms-excel');
      header('Content-Disposition: attachment;filename="DataPenilaian.xlsx"');
      header('Cache-Control: max-age=0');

      PhpExcelTemplator::saveToFile('./template.xlsx', 'php://output', [
        '{pelajaran}' => $datas['pelajaran'],
        '{jurusan}' => $datas['jurusan'],
        '{kelas}' => $datas['kelas'],
        '{semester}' => $datas['semester'],
        '{tahun_ajaran}' => $datas['tahun_ajaran'],
        '[nomor]' => $nomor,
        '[no_induk]' => $no_induk,
        '[nama_siswa]' => $nama_siswa,
        '[nilai_kkm]' => $nilai_kkm,
        '[nilai]' => $nilai,
      ]);
    }else{
      $pdf = new FPDF('l','mm','A5');
        // membuat halaman baru
      $pdf->AddPage();
        // setting jenis font yang akan digunakan
      $pdf->SetFont('Arial','B',16);
        // mencetak string 
      $pdf->Cell(190,7,'SEKOLAH MENENGAH KEJURUSAN MUHAMMADIYAH KAJEN',0,1,'C');
      $pdf->SetFont('Arial','B',12);
      $tahun_ajaran = $datas['tahun_ajaran'];
      $semester = strtoupper($datas['semester']);
      $mata_pelajaran = $datas['pelajaran'];
      $jurusan = $datas['jurusan'];
      $kelas = $datas['kelas'];
      $pdf->Cell(190,7,"DATA PENILAIAN PESERTA DIDIK TAHUN AJARAN $tahun_ajaran",0,1,'C');
      $pdf->Cell(190,7,"$semester",0,1,'C');
        // Memberikan space kebawah agar tidak terlalu rapat
      $pdf->Cell(10,7,'',0,1);

      $pdf->SetFont('Arial','',10);
      $pdf->Cell(10,6,"Mata Pelajaran : $mata_pelajaran",0,1);
      $pdf->Cell(10,6,"Jurusan : $jurusan",0,1);
      $pdf->Cell(100,6,"Kelas : $kelas",0,1);
      $pdf->Cell(10,7,'',0,1);
      $pdf->SetFont('Arial','B',10);
      $pdf->Cell(10,6,'No.',1,0);
      $pdf->Cell(20,6,'No. Induk',1,0);
      $pdf->Cell(110,6,'Nama Siswa',1,0);
      $pdf->Cell(27,6,'Nilai KKM',1,0);
      $pdf->Cell(25,6,'Nilai',1,1);
      $pdf->SetFont('Arial','',10);
      $no = 1;

      foreach ($datas['user'] as $key) {
        $no_induk = $this->db->get_where('users', ['id' => $key['nilai']->id_users])->row()->no_induk;
        $pdf->Cell(10,6,$no++,1,0);
        $pdf->Cell(20,6,$no_induk,1,0);
        $pdf->Cell(110,6,$key['nama'],1,0);
        $pdf->Cell(27,6,$key['nilai']->kkm,1,0);
        $pdf->Cell(25,6,$key['nilai']->nilai,1,1); 
      }
      $pdf->Output('D', 'DataPenilaian.pdf');
    }

  }

  public function nilai_by_siswa($id_semester = 1)
  {
    $id = $this->session->userdata('sess_id');
    $datas = $this->Nilai->getNilaiId($id, $id_semester);

    $nomor = [];
    $mata_pelajaran = [];
    $kelas = [];
    $tahun_ajaran = [];
    $nilai_kkm = [];
    $nilai = [];
    $i=1;
    foreach ($datas['user'] as $key) {
      $nomor[] = $i;
      $mata_pelajaran[] = $key['pelajaran'];
      $tahun_ajaran[] = $key['tahun_ajaran_nilai'];
      $kelas[] = $key['kelas_nilai'];
      $nilai_kkm[] = $key['nilai']->kkm;
      $nilai[] = $key['nilai']->nilai;
      $i++;
    }
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="DataPenilaian.xlsx"');
    header('Cache-Control: max-age=0');

    PhpExcelTemplator::saveToFile('./template_siswa.xlsx', 'php://output', [
      '{nama_siswa}' => $datas['nama'],
      '{jurusan}' => $datas['jurusan'],
      '{kelas}' => $datas['kelas'],
      '{semester}' => $datas['semester'],
      '{tahun_ajaran}' => $datas['tahun_ajaran'],
      '[nomor]' => $nomor,
      '[mata_pelajaran]' => $mata_pelajaran,
      '[kelas]' => $kelas,
      '[tahun_ajaran]' => $tahun_ajaran,
      '[nilai_kkm]' => $nilai_kkm,
      '[nilai]' => $nilai,
    ]);
  }

  public function nilai_persiswa($id_kelas, $id_semester, $id_users, $id_tahun_ajaran)
  {
    $data = $this->Nilai->getNilaiAllMapelBySiswa($id_kelas, $id_semester, $id_users, $id_tahun_ajaran);
    $data = (object) $data;
    $pdf = new FPDF('l','mm','A5');
        // membuat halaman baru
    $pdf->AddPage();
        // setting jenis font yang akan digunakan
        // mencetak string 
    $pdf->SetFont('Arial','',10);

    $pdf->Cell(10,6,"Nama                                  : $data->nama_siswa",0,1);
    $pdf->Cell(10,6,"NIS / NISN                          : $data->no_induk",0,1);
    $pdf->Cell(100,6,"Kelas                                   : $data->kelas",0,1);
    $pdf->Cell(100,6,"Semester / Tahun Ajaran    : $data->semester / $data->tahun_ajaran",0,1);
    $pdf->Line(10, 35, 220-20, 35);
    $pdf->Line(60, 35, 220-50, 35);
    $pdf->Cell(5,7,'',0,1);
    $pdf->SetFont('Arial','B',12);
    $pdf->Cell(190,7,'PENCAPAIAN KOMPETENSI PESERTA DIDIK',0,1,'C');
        // Memberikan space kebawah agar tidak terlalu rapat
    $pdf->Cell(10,7,'',0,1);

    
    $pdf->SetFont('Arial','B',10);
    $pdf->Cell(100,6,"A. Nilai Akademik",0,1);
    $pdf->Cell(10,6,'No.',1,0);
    $pdf->Cell(90,6,'Mata Pelajaran',1,0);
    $pdf->Cell(40,6,'Nilai KKM',1,0);
    $pdf->Cell(27,6,'Nilai',1,0);
    $pdf->Cell(25,6,'Predikat',1,1);
    $pdf->SetFont('Arial','',10);
    $no = 1;

    foreach ($data->nilai as $key) {
      $pdf->Cell(10,6,$no++,1,0);
      $pdf->Cell(90,6,$key['pelajaran'],1,0);
      $pdf->Cell(40,6,$key['kkm'],1,0);
      $pdf->Cell(27,6,$key['nilai'],1,0);
      $pdf->Cell(25,6,$key['predikat'],1,1); 
    }
    $pdf->Output('D', 'DataPenilaianSiswa.pdf');
  }
}