<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if (!auth_check()) {
			redirect('auth/login');
		}
		if ($this->session->userdata('sess_role') == 'siswa') {
			redirect('.');
		}
		$this->load->model('Users_model', 'Users');
		$this->load->model('Akademik_model', 'Akademik');
	}

	public function siswa()
	{
		$data = [
			'title' => 'Data User ~ Siswa',
			'users' => $this->Users->get_user('siswa'),
			'jurusan' => $this->Akademik->get('jurusan'),
			'kelas' => $this->Akademik->get('kelas'),
		];
		$this->load->view('pages/user/siswa', $data);
	}

	public function guru()
	{
		$data = [
			'title' => 'Data User ~ Guru',
			'users' => $this->Users->get_user('guru'),
		];
		$this->load->view('pages/user/guru', $data);
	}

	public function admin()
	{
		if ($this->session->userdata('sess_role') == 'guru') {
			redirect('.');
		}
		$data = [
			'title' => 'Data User ~ Administrator',
			'users' => $this->Users->get_user('admin'),
		];
		$this->load->view('pages/user/admin', $data);
	}

	public function get_user($id)
	{
		$data = $this->Users->detail($id);
		echo json_encode($data);
	}

	public function update()
	{
		$data = $this->input->post();
		$this->Users->update_user($data);
		$this->session->set_flashdata('msg', 'update');
		redirect($this->agent->referrer());
	}

	public function create($role)
	{
		$data = (object) $this->input->post();

		$data->username = str_replace(' ', '_', strtolower($data->nama));
		$data->password = str_replace('-', '', strtolower(date("d-m-Y", strtotime($data->tgl_lahir))));
		$data->role = $role;
		$data->password = md5($data->password);
		$insert = $this->Users->insert_user($data);
		if (!$insert) {
			$this->session->set_flashdata('msg', 'failed');
			redirect($this->agent->referrer());
		}else{
			$this->session->set_flashdata('msg', 'success');
			redirect($this->agent->referrer());
		}
	}

	public function delete($id)
	{
		$this->Users->delete_user($id);
		$this->session->set_flashdata('msg', 'delete');
		redirect($this->agent->referrer());
	}

	public function ubah_password()
	{
		$id = $this->input->post('id_users_pw');
		$old_password = $this->input->post('old_password');
		$password = $this->input->post('password');

		$data = [
			'id' => $id,
			'old_password' => $old_password,
			'password' => $password,
		];

		$return = $this->Users->update_pass($data);
		if ($return) {
			$this->session->set_flashdata('msg', 'ubah_password');
		}else{
			$this->session->set_flashdata('msg', 'ubah_password_fail');
		}
		redirect($this->agent->referrer());

	}
}
