<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Nilai extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if (!auth_check()) {
			redirect('auth/login');
		}
		$this->load->model('Nilai_model', 'Nilai');
		$this->load->model('Akademik_model', 'Akademik');
		$this->load->model('Users_model', 'Users');
	}

	public function index()
	{
		if ($this->session->userdata('sess_role') != 'guru') {
			redirect('.');
		}
		$data = [
			'title' => 'Nilai ~ Index',
			'jurusan' => $this->Akademik->get('jurusan'),
			'kelas' => $this->Akademik->get('kelas'),
			'pelajaran' => $this->db->get_where('pelajaran', ['id' => $this->session->userdata('sess_id_pelajaran')])->result(),
			'semester' => $this->Akademik->get('semester'),
			'tahun_ajaran' => $this->Akademik->get('tahun_ajaran'),
		];
		$this->load->view('pages/nilai/nilai', $data);
	}

	public function persiswa()
	{
		$data = [
			'title' => 'Nilai Siswa ~ Index',
			'kelas' => $this->Akademik->get('kelas'),
			'siswa' => $this->db->get_where('users', ['role' => 'siswa'])->result(),
			'pelajaran' => $this->db->get_where('pelajaran', ['id' => $this->session->userdata('sess_id_pelajaran')])->result(),
			'semester' => $this->Akademik->get('semester'),
			'tahun_ajaran' => $this->Akademik->get('tahun_ajaran'),
		];
		$this->load->view('pages/nilai/nilai_persiswa', $data);
	}

	public function get_nilai_all_mapel($id_kelas, $id_semester, $id_users, $id_tahun_ajaran)
	{
		$data = $this->Nilai->getNilaiAllMapelBySiswa($id_kelas, $id_semester, $id_users, $id_tahun_ajaran);
		echo json_encode($data);
	}

	public function input()
	{
		if ($this->session->userdata('sess_role') != 'guru') {
			redirect('.');
		}
		if (empty($this->input->post())) {
			redirect('nilai');
		}
		$data = (object) $this->input->post();
		
		$data = [
			'title' => 'Nilai ~ Input Nilai',
			'id_jurusan' =>  explode('/nxa/', $data->id_jurusan)[0],
			'jurusan' =>  explode('/nxa/', $data->id_jurusan)[1],
			'id_kelas' =>  explode('/nxa/', $data->id_kelas)[0],
			'kelas' =>  explode('/nxa/', $data->id_kelas)[1],
			'id_pelajaran' =>  explode('/nxa/', $data->id_pelajaran)[0],
			'pelajaran' =>  explode('/nxa/', $data->id_pelajaran)[1],
			'id_semester' => explode('/nxa/', $data->id_semester)[0],
			'semester' => explode('/nxa/', $data->id_semester)[1],
			'id_tahun_ajaran' => explode('/nxa/', $data->id_tahun_ajaran)[0],
			'tahun_ajaran' => explode('/nxa/', $data->id_tahun_ajaran)[1],
			'kkm' => $data->kkm,
		];
		$data['siswa'] = $this->Nilai->getUserbyJurusan($data);
		if (count($data['siswa']) == 0) {
			$this->session->set_flashdata('msg', 'no_data');
			redirect('nilai');
		}
		$this->load->view('pages/nilai/nilai_tampil', $data);
	}

	public function simpan_nilai()
	{
		if ($this->session->userdata('sess_role') != 'guru') {
			redirect('.');
		}
		$data = (object) $this->input->post();
		$return = $this->Nilai->submit_nilai($data);
		if ($return) {
			$this->session->set_flashdata('msg', 'nilai_success');
		}else{
			$this->session->set_flashdata('msg', 'nilai_fail');
		}

		$url = base64_encode($data->id_jurusan."|".$data->id_kelas."|".$data->id_pelajaran."|".$data->id_semester."|".$data->id_tahun_ajaran);
		
		redirect('nilai/daftar/'.$url);
	}

	public function daftar($data)
	{
		if ($this->session->userdata('sess_role') !=  'guru') {
			redirect('.');
		}
		$data = explode("|", base64_decode($data));
		
		$datas = $this->Nilai->getUserNilai($data);
		if (count($datas['user']) == 0) {
			$this->session->set_flashdata('msg', 'no_data');
			redirect('nilai');
		}
		$this->load->view('pages/nilai/daftar_nilai', $datas);

	}

	public function detail($id)
	{
		$data = $this->Nilai->detail($id);
		echo json_encode($data);
	}


	public function update()
	{
		$data = $this->input->post();
		$return = $this->Nilai->update_nilai($data);

		$this->session->set_flashdata('msg', 'update');
		redirect($this->agent->referrer());
	}

	public function delete($id)
	{
		$this->Nilai->delete($id);

		$this->session->set_flashdata('msg', 'delete');
		redirect($this->agent->referrer());
	}

	public function check($id_semester = 1)
	{
		$id = $this->session->userdata('sess_id');
		$datas = $this->Nilai->getNilaiId($id, $id_semester);
		// if (count($datas['user']) == 0) {
		// 	$this->session->set_flashdata('msg', 'no_data');
		// 	redirect('home');
		// }
		$this->load->view('pages/nilai/check_nilai', $datas);
	}


	public function grafik_nilai_rata2()
	{
		$id_users = $this->session->userdata('sess_id');
		$data = $this->Nilai->get_grafik($id_users);
		echo json_encode($data);
	}
}
