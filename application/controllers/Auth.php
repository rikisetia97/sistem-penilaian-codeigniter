<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	function __construct()
	{
        // Construct the parent class
		parent::__construct();
		$this->load->model('Auth_model', 'Auth');
		
	}

	public function login()
	{
		if (auth_check()) {
			redirect();
		}
		$this->load->view('pages/login');
	}

	public function auth_login()
	{
		if (auth_check()) {
			redirect();
		}
		$username = $this->input->post('username', true);
		$password = $this->input->post('password', true);
		$result = $this->Auth->check_login($username,$password);

		if ($result->role == 'siswa') {
			$this->session->set_flashdata('errors', 'Saat ini siswa tidak dapat login!');
			redirect($this->agent->referrer());
			die;
		}

		if ($result) {
			$user_data = array(
				'sess_id' => $result->id,
				'sess_username' => $result->username,
				'sess_nama' => $result->nama,
				'sess_email' => $result->email,
				'sess_role' => $result->role,
				'sess_id_pelajaran' => $result->id_pelajaran,
				'sess_logged_in' => true,
			);                
			$this->session->set_userdata($user_data);
			redirect('home');
		} else {
			$this->session->set_flashdata('errors', 'Username / Password salah!');
			redirect($this->agent->referrer());
		}
	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect('auth/login');
	}
}
