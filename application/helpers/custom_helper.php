<?php


defined('BASEPATH') OR exit('No direct script access allowed');


if (!function_exists('auth_check')) {
    function auth_check()
    {
        // Get a reference to the controller object
        $ci =& get_instance();
        $ci->load->model('Auth_model');
        return $ci->Auth_model->is_logged_in();
    }
}

