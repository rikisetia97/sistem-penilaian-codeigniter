<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Nilai_model extends CI_Model
{
	
	// SUBMIT DATA NILAI
	public function submit_nilai($data)
	{
		$array = array_combine($data->id_users, $data->nilai);
		$insert_nilai = [];
		$i = 0;
		foreach ($array as $key => $value) {
			$insert_nilai[$i]['id_jurusan'] = $data->id_jurusan;
			$insert_nilai[$i]['id_kelas'] = $data->id_kelas;
			$insert_nilai[$i]['id_pelajaran'] = $data->id_pelajaran;
			$insert_nilai[$i]['id_semester'] = $data->id_semester;
			$insert_nilai[$i]['id_tahun_ajaran'] = $data->id_tahun_ajaran;
			$insert_nilai[$i]['id_users'] = $key;

			$checkDuplicate = $this->db->get_where('nilai', $insert_nilai[$i])->row();
			$insert_nilai[$i]['kkm'] = $data->kkm;
			$insert_nilai[$i]['nilai'] = $value;
			if (empty($checkDuplicate)) {
				$insert = $this->db->insert('nilai', $insert_nilai[$i]);
			}
			$i++;
		}
		return $insert;
	}

	public function getUserbyJurusan($data)
	{
		$data = (object) $data;
		$query = $this->db->get_where('users', ['id_jurusan' => $data->id_jurusan, 'id_kelas' => $data->id_kelas])->result();

		$users = [];
		$i = 0;
		foreach ($query as $key) {
			$checkDuplicate = $this->db->get_where('nilai', [
				'id_jurusan' => $data->id_jurusan, 
				'id_kelas' => $data->id_kelas,
				'id_pelajaran' => $data->id_pelajaran,
				'id_semester' => $data->id_semester,
				'id_tahun_ajaran' => $data->id_tahun_ajaran,
				'id_users' => $key->id,
			])->row();
			if (empty($checkDuplicate)) {
				$users[$i] = $key;
				$i++;
			}
		}
		return $users;
	}

	public function getUserNilai($data)
	{
		$id_jurusan = $data[0];
		$id_kelas = $data[1];
		$id_pelajaran = $data[2];
		$id_semester = $data[3];
		$id_tahun_ajaran = $data[4];

		$user_nilai = $this->db->get_where('nilai', [
			'id_jurusan' => $id_jurusan,
			'id_pelajaran' => $id_pelajaran,
			'id_kelas' => $id_kelas,
			'id_semester' => $id_semester,
			'id_tahun_ajaran' => $id_tahun_ajaran,
		])->result();

		$users = [];
		$temp = [];
		$i = 0;
		$users['title'] = 'Nilai ~ Daftar Nilai';
		$users['jurusan'] = $this->db->get_where('jurusan', ['id' => $id_jurusan])->row()->jurusan;
		$users['kelas'] = $this->db->get_where('kelas', ['id' => $id_kelas])->row()->kelas;
		$users['pelajaran'] = $this->db->get_where('pelajaran', ['id' => $id_pelajaran])->row()->mata_pelajaran;
		$users['semester'] = $this->db->get_where('semester', ['id' => $id_semester])->row()->semester;
		$users['tahun_ajaran'] = $this->db->get_where('tahun_ajaran', ['id' => $id_tahun_ajaran])->row()->tahun_ajaran;
		foreach ($user_nilai as $key) {
			$temp[$i]['nama'] = $this->db->get_where('users', [
				'id' => $key->id_users, 
			])->row()->nama;
			$temp[$i]['nilai'] = $key;
			$i++;
		}
		$users['user'] = $temp;
		return $users;
	}


	public function getNilaiId($id,$id_semester)
	{
		$user_nilai = array_reverse($this->db->get_where('nilai', ['id_users' => $id, 'id_semester' => $id_semester])->result());
		$users = [];
		$temp = [];
		$i = 0;
		$users['title'] = 'Check Nilai';
		$users['jurusan'] = $this->db->get_where('jurusan', ['id' => $user_nilai[0]->id_jurusan])->row()->jurusan;
		$users['kelas'] = $this->db->get_where('kelas', ['id' => $user_nilai[0]->id_kelas])->row()->kelas;
		$users['nama'] = $this->db->get_where('users', ['id' => $user_nilai[0]->id_users])->row()->nama;
		$users['semester'] = $this->db->get_where('semester', ['id' => $id_semester])->row()->semester;
		$users['tahun_ajaran'] = $this->db->get_where('tahun_ajaran', ['id' => $user_nilai[0]->id_tahun_ajaran])->row()->tahun_ajaran;
		foreach ($user_nilai as $key) {
			$temp[$i]['pelajaran'] = $this->db->get_where('pelajaran', [
				'id' => $key->id_pelajaran, 
			])->row()->mata_pelajaran;
			$temp[$i]['kelas_nilai'] = $this->db->get_where('kelas', [
				'id' => $key->id_kelas, 
			])->row()->kelas;
			$temp[$i]['tahun_ajaran_nilai'] = $this->db->get_where('tahun_ajaran', [
				'id' => $key->id_tahun_ajaran, 
			])->row()->tahun_ajaran;
			$temp[$i]['nilai'] = $key;
			$i++;
		}
		$users['user'] = $temp;
		$users['get_semester'] = $this->db->get('semester')->result();

		return $users;
	}

	public function detail($id)
	{
		$query = $this->db->get_where('nilai', ['id' => $id])->row();
		return $query;
	}

	public function update_nilai($data)
	{
		$data = (object) $data;
		
		$this->db->set('nilai', $data->ed_nilai);
		$this->db->where('id', $data->ed_id);
		$this->db->update('nilai');
		return true;
	}

	public function delete($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('nilai');
		return true;
	}

	public function get_grafik($id_users)
	{
		$this->db->select('id_kelas, id_semester');
		$this->db->group_by('id_kelas');
		$this->db->group_by('id_semester');
		$query = $this->db->get_where('nilai', ['id_users' => $id_users])->result();
		$label = [];
		$nilai = [];
		$result = [];
		$nilarata2 = [];
		foreach ($query as $key) {
			$kelas = explode(" ", $this->db->get_where('kelas', ['id' => $key->id_kelas])->row()->kelas)[0];
			$semester = $this->db->get_where('semester', ['id' => $key->id_semester])->row()->semester;
			$label[] = $kelas." - ".$semester; 	
			$this->db->select('nilai');
			$rata2 = $this->db->get_where('nilai', ['id_kelas' => $key->id_kelas, 'id_semester' => $key->id_semester])->result();
			foreach ($rata2 as $key) {
				$nilarata2[] = $key->nilai;
			}
			$nilai[] = array_sum($nilarata2)/count($nilarata2); 	
		}
		$result['label'] = $label;
		$result['nilai'] = $nilai;
		return $result;
	}

	public function getNilaiAllMapelBySiswa($id_kelas, $id_semester, $id_users, $id_tahun_ajaran)
	{
		$this->db->where('id_kelas', $id_kelas);
		$this->db->where('id_semester', $id_semester);
		$this->db->where('id_users', $id_users);
		$query = $this->db->get('nilai')->result();
		$data = [];
		$result = [];
		$siswa = $this->db->get_where('users', ['id' => $id_users])->row();
		$result['kelas'] = $this->db->get_where('kelas', ['id' => $id_kelas])->row()->kelas;
		$result['nama_siswa'] = $siswa->nama;
		$result['no_induk'] = $siswa->no_induk;
		$result['semester'] = $this->db->get_where('semester', ['id' => $id_semester])->row()->semester;
		$result['tahun_ajaran'] = $this->db->get_where('tahun_ajaran', ['id' => $id_tahun_ajaran])->row()->tahun_ajaran;
		foreach ($query as $item) {
			$data['pelajaran'] = $this->db->get_where('pelajaran', ['id' => $item->id_pelajaran])->row()->mata_pelajaran;
			$data['kkm'] = $item->kkm;
			$data['nilai'] = $item->nilai;
			if ($item->nilai>=85 &&$item->nilai<=100) {
				$predikat = 'A';
			}
			else if ($item->nilai>=75 &&$item->nilai<85){
				$predikat ='B';
			}
			else if ($item->nilai>=60 &&$item->nilai<75){
				$predikat ='C';
			}
			else if ($item->nilai<60){
				$predikat = 'D';
			}
			$data['predikat'] = $predikat;
			$result['nilai'][] = $data;
		}
		return $result;
	}

}
