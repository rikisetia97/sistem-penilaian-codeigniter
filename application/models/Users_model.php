<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Users_model extends CI_Model
{
	
	public function get_user($role)
	{
		if ($role == 'siswa') {
			$this->db->select('u.*, j.id id_jurusan, j.jurusan, k.id id_kelas, k.kelas');
			$this->db->join('jurusan j', 'j.id=u.id_jurusan');
			$this->db->join('kelas k', 'k.id=u.id_kelas');
		}
		$query = $this->db->get_where('users u', ['u.role' => $role])->result();

		return $query;
		
	}

	public function detail($id)
	{
		$query = $this->db->get_where('users', ['id' => $id])->row();
		return $query;
	}

	public function get_by_jurusan($id_jurusan, $id_kelas)
	{
		$query = $this->db->get_where('users', ['id_jurusan' => $id_jurusan, 'id_kelas' => $id_kelas])->result();
		return $query;
	}


	public function insert_user($data)
	{
		$checkDuplicate = $this->db->get_where('users', ['username' => $data->username, 'password' => $data->password])->row();
		if ($checkDuplicate) {
			return false;
		}else{
			$this->db->insert('users', $data);
			return true;
		}

	}

	public function update_user($data)
	{
		$data = (object) $data;
		$data->ed_tgl_lahir = date("Y-m-d", strtotime($data->ed_tgl_lahir));
		$this->db->set('nama', $data->ed_nama);
		$this->db->set('tgl_lahir', $data->ed_tgl_lahir);
		$this->db->set('alamat', $data->ed_alamat);
		$this->db->set('jenis_kelamin', $data->ed_jenis_kelamin);
		$this->db->set('no_induk', $data->ed_no_induk);
		$this->db->set('id_jurusan', $data->ed_id_jurusan);
		$this->db->set('id_kelas', $data->ed_id_kelas);
		$this->db->set('email', $data->ed_email);
		$this->db->set('no_telpon', $data->ed_no_telpon);
		$this->db->set('id_pelajaran', $data->ed_id_pelajaran);
		$this->db->where('id', $data->ed_id);
		$this->db->update('users');
		return true;

	}

	public function update_pass($data)
	{
		$data = (object) $data;
		$this->db->set('password', md5($data->password));
		$this->db->where('id', $data->id);
		$this->db->where('password', md5($data->old_password));
		$this->db->update('users');

		return $this->db->affected_rows();

	}

	public function delete_user($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('users');
		
		return true;
	}


}
