<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Akademik_model extends CI_Model
{
	
	// MULTIPLE QUERY
	// GET DATA
	public function get($table)
	{
		$query = $this->db->get($table)->result();

		return $query;
	}

	// DELETE DATA
	public function delete($id, $table)
	{
		$this->db->where('id', $id);
		$this->db->delete($table);
		return true;
	}

	// GET BY DETAIL DATA
	public function detail($id, $table)
	{
		$query = $this->db->get_where($table, ['id' => $id])->row();
		return $query;
	}

	// SINGLE QUERY
	// INSERT DATA JURUSAN
	public function insert_jurusan($data)
	{
		$checkDuplicate = $this->db->get_where('jurusan', ['jurusan' => $data->jurusan])->row();
		if ($checkDuplicate) {
			return false;
		}else{
			$this->db->insert('jurusan', $data);
			return true;
		}
	}

	// INSERT DATA PELAJARAN
	public function insert_pelajaran($data)
	{
		$checkDuplicate = $this->db->get_where('pelajaran', ['mata_pelajaran' => $data->mata_pelajaran])->row();
		if ($checkDuplicate) {
			return false;
		}else{
			$this->db->insert('pelajaran', $data);
			return true;
		}
	}

	// INSERT DATA PELAJARAN
	public function insert_kelas($data)
	{
		$checkDuplicate = $this->db->get_where('kelas', ['kelas' => $data->kelas])->row();
		if ($checkDuplicate) {
			return false;
		}else{
			$this->db->insert('kelas', $data);
			return true;
		}
	}

	// INSERT DATA TAHUN AJARAN
	public function insert_tahun_ajaran($data)
	{
		$checkDuplicate = $this->db->get_where('tahun_ajaran', ['tahun_ajaran' => $data->tahun_ajaran])->row();
		if ($checkDuplicate) {
			return false;
		}else{
			$this->db->insert('tahun_ajaran', $data);
			return true;
		}
	}


	// UPDATE DATA JURUSAN
	public function update_jurusan($data)
	{
		$data = (object) $data;
		$checkDuplicate = $this->db->get_where('jurusan', ['jurusan' => $data->ed_jurusan])->row();
		if ($checkDuplicate) {
			return false;
		}else{
			$this->db->set('jurusan', $data->ed_jurusan);
			$this->db->where('id', $data->ed_id);
			$this->db->update('jurusan');
			return true;
		}
	}

	// UPDATE DATA PELAJARAN
	public function update_pelajaran($data)
	{
		$data = (object) $data;
		$checkDuplicate = $this->db->get_where('pelajaran', ['mata_pelajaran' => $data->ed_mata_pelajaran])->row();
		if ($checkDuplicate) {
			return false;
		}else{
			$this->db->set('mata_pelajaran', $data->ed_mata_pelajaran);
			$this->db->where('id', $data->ed_id);
			$this->db->update('pelajaran');
			return true;
		}
	}

	// UPDATE DATA TAHUN AJARAN
	public function update_tahun_ajaran($data)
	{
		$data = (object) $data;
		$checkDuplicate = $this->db->get_where('tahun_ajaran', ['tahun_ajaran' => $data->ed_tahun_ajaran])->row();
		if ($checkDuplicate) {
			return false;
		}else{
			$this->db->set('tahun_ajaran', $data->ed_tahun_ajaran);
			$this->db->where('id', $data->ed_id);
			$this->db->update('tahun_ajaran');
			return true;
		}
	}


}
