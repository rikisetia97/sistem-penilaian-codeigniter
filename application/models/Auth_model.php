<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Auth_model extends CI_Model
{
	public function is_logged_in()
	{
		if ($this->session->userdata('sess_logged_in') == true) 
		{
			return true;
		}
		return false;
	}
	
	public function check_login($username,$password)
	{
		$query = $this->db->get_where('users', ['username' => $username, 'password' => md5($password)])->row();

        return $query;
		
	}

	public function logout()
	{
		$this->session->sess_destroy();
	}
}
