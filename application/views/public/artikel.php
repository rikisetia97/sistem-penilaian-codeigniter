<?php $this->load->view('templates/header') ?>
<?php $this->load->view('templates/sidebar') ?>
<?php $this->load->view('templates/navbar') ?>


<!-- Data Siswa -->

<div class="row mt-3">
  <div class="col-lg-12">
    <div class="card mb-4">
      <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
        <h6 class="m-0 font-weight-bold text-primary">Daftar Berita/Artikel</h6>
        <a href="#" class="btn btn-success btn-icon-split "  data-toggle="dropdown">
          <span class="icon text-white-50">
            <i class="fas fa-filter"></i>
          </span>
          <span class="text">Filter Berita</span>
        </a>
        <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 38px, 0px);">
          <a class="dropdown-item" href="#">Terbaru</a>
          <a class="dropdown-item" href="#">Sering Dilihat</a>
          <a class="dropdown-item" href="#">Paling Lawas</a>
        </div>

      </div>
    </div>
  </div>
  <div class="col-lg-12">
    <div class="card mb-4">
      <div class="card-body">
        <a href="#"><h6 class="m-0 font-weight-bold mb-3">SMK Muhamka Selenggarakan PTS Gasal 2020/221 Secara Daring</h6></a>
        <p style="font-size: 12px; margin-top: -12px">Author : <b>Admin Sekolah</b><br> Diposting pada tanggal : 20 September 2020</p>
        <img src="<?=base_url('assets/gambar/dashboard.jpg');?>" class="img-fluid">
        <p class="text-justify" style="text-indent: 20px">Pembelajaran tahun pelajaran 2020/2021 yang dimulai sejak bulan Juli lalu dilaksanakan dalam jaringan (daring) atau Pembelajaran Jarak Jauh (PJJ) . . .  </p>
        <a href="<?=base_url('auth/login');?>" class="btn btn-success btn-block">Baca lebih lanjut <i class="fa fa-arrow-right"></i></a>
      </div>
    </div>
  </div>

</div>
<?php $this->load->view('templates/footer') ?>
