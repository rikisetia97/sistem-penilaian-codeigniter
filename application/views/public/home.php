<?php $this->load->view('templates/header') ?>
<?php $this->load->view('templates/sidebar') ?>
<?php $this->load->view('templates/navbar') ?>


<!-- Data Siswa -->
<img src="<?=base_url('assets/gambar/dashboard.jpg');?>" class="img-fluid">

<div class="row mt-3">
  <div class="col-lg-12">
    <div class="card mb-4">
      <div class="card-body">
        <h6 class="text-center"><span style="font-style: italic;">Selamat Datang di <br> Sistem Penilaian Akademik</span><br><span style="font-weight: bold">SMK MUHAMMADIYAH KAJEN</span></h6>
        <div class="text-center">
          <img src="<?=base_url('assets/gambar/logo.png');?>" class="img-fluid">
          <p>SMK Muhammadiyah Kajen Kabupaten Pekalongan, didirikan Pimpinan Cabang Muhammadiyah Kajen Kabupaten Pekalongan tahun 2002. Para tokoh pendidikan dan kalangan muda bermimpi, di Kajen harus ada Sekolah Kejuruan, melengkapi sekolah/madrasah yang ada.</p>
        </div>
      </div>
    </div>
    <div class="card mb-4">
      <div class="card-body">
        <a href="<?=base_url('auth/login');?>" class="btn btn-primary btn-block"><i class="fas fa-fw fa-sign-in-alt"></i> Klik disini untuk masuk ke sistem</a>
      </div>
    </div>
  </div>
</div>
<?php $this->load->view('templates/footer') ?>
