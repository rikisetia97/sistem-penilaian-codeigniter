<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <link href="<?=base_url('assets/gambar/logo.png');?>" rel="icon">
  <title><?=$title;?></title>
  <link href="<?=base_url('assets/vendor/fontawesome-free/css/all.min.css');?>" rel="stylesheet" type="text/css">
  <link href="<?=base_url('assets/vendor/bootstrap/css/bootstrap.css');?>" rel="stylesheet" type="text/css">
  <link href="<?=base_url('assets/vendor/datatables/dataTables.bootstrap4.min.css');?>" rel="stylesheet">
  <link href="<?=base_url('assets/css/ruang-admin.min.css');?>" rel="stylesheet">
  <link href="<?=base_url('assets/vendor/bootstrap-datepicker/css/bootstrap-datepicker.min.css');?>" rel="stylesheet" >
  <link href="<?=base_url('assets/vendor/select2/dist/css/select2.css');?>" rel="stylesheet" type="text/css">
  <script src="<?=base_url('assets/vendor/jquery/jquery.min.js');?>"></script>
  <script src="<?=base_url('assets/vendor/bootstrap/js/bootstrap.bundle.min.js');?>"></script>
  <script src="<?=base_url('assets/vendor/jquery-easing/jquery.easing.min.js');?>"></script>
  <script src="<?=base_url('assets/vendor/datatables/jquery.dataTables.min.js');?>"></script>
  <script src="<?=base_url('assets/vendor/datatables/dataTables.bootstrap4.min.js');?>"></script>
  <script src="<?=base_url('assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js');?>"></script>
  <script src="<?=base_url('assets/vendor/select2/dist/js/select2.min.js');?>"></script>
  <script src="<?=base_url('assets/vendor/chart.js/Chart.min.js');?>"></script>
</head>
