<body id="page-top">
  <div id="wrapper">
    <!-- Sidebar -->
    <ul class="navbar-nav sidebar sidebar-light accordion" id="accordionSidebar">
      <a class="sidebar-brand bg-gradient-primary d-flex align-items-center justify-content-center" href="<?=base_url('.');?>">
        <div class="sidebar-brand-icon">
          <img src="<?=base_url('assets/gambar/logo.png');?>">
        </div>
        <div class="sidebar-brand-text mx-2">SMK MUHAMKA</div>
      </a>
      <hr class="sidebar-divider my-0">
      <!-- ADMINISTRATOR -->
      <?php if ($this->session->userdata('sess_role') == 'admin') { ?>
        <li class="nav-item <?php if($this->uri->segment(1)=='home' || $this->uri->segment(1)==''){echo 'active';}?>">
          <a class="nav-link" href="<?=base_url('.');?>">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span></a>
          </li>

          <hr class="sidebar-divider">
          <div class="sidebar-heading">
            Select Menu
          </div>

          <li class="nav-item <?php if($this->uri->segment(2)=='siswa' || $this->uri->segment(2)=='guru' || $this->uri->segment(2)=='admin'){echo 'active';}?>">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#data_user"
            aria-expanded="true" aria-controls="data_user">
            <i class="fa fa-users"></i>
            <span>Data User</span>
          </a>
          <div id="data_user" class="collapse" aria-labelledby="headingBootstrap" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
              <h6 class="collapse-header">Pilih User</h6>
              <a class="collapse-item <?php if($this->uri->segment(2)=='admin'){echo 'active';}?>" href="<?=base_url('users/admin');?>">Administrator</a>
              <a class="collapse-item <?php if($this->uri->segment(2)=='guru'){echo 'active';}?>" href="<?=base_url('users/guru');?>">Guru</a>
              <a class="collapse-item <?php if($this->uri->segment(2)=='siswa'){echo 'active';}?>" href="<?=base_url('users/siswa');?>">Siswa</a>
            </div>
          </div>
        </li>

        <li class="nav-item">
          <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#data_akademik"
          aria-expanded="true" aria-controls="data_akademik">
          <i class="fa fa-briefcase"></i>
          <span>Data Akademik</span>
        </a>
        <div id="data_akademik" class="collapse" aria-labelledby="headingBootstrap" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Pilih Akademik</h6>
            <a class="collapse-item" href="<?=base_url('akademik/jurusan');?>">Jurusan</a>
            <a class="collapse-item" href="<?=base_url('akademik/kelas');?>">Kelas</a>
            <a class="collapse-item" href="<?=base_url('akademik/pelajaran');?>">Mata Pelajaran</a>
            <a class="collapse-item" href="<?=base_url('akademik/tahun_ajaran');?>">Tahun Ajaran</a>
          </div>
        </div>
      </li>

      <li class="nav-item">
        <a class="nav-link" href="javascript:void(0);" data-toggle="modal" data-target="#logoutModal">
          <i class="fas fa-fw fa-sign-out-alt"></i>
          <span>Keluar</span>
        </a>
      </li>
<!--       <li class="nav-item <?php if($this->uri->segment(1)=='pengaturan'){echo 'active';}?>">
        <a class="nav-link" href="<?=base_url('pengaturan');?>">
          <i class="fas fa-fw fa-cogs"></i>
          <span>Pengaturan</span>
        </a>
      </li> -->

      <!-- GURU -->

    <?php }else if($this->session->userdata('sess_role') == 'guru'){ ?>

      <li class="nav-item <?php if($this->uri->segment(1)=='home' || $this->uri->segment(1)==''){echo 'active';}?>">
        <a class="nav-link" href="<?=base_url('.');?>">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Dashboard</span></a>
        </li>

        <hr class="sidebar-divider">
        <div class="sidebar-heading">
          Select Menu
        </div>

        <li class="nav-item">
          <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#data_nilai"
          aria-expanded="true" aria-controls="data_nilai">
          <i class="fa fa-laptop-code"></i>
          <span>Nilai</span>
        </a>
        <div id="data_nilai" class="collapse" aria-labelledby="headingBootstrap" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Pilih Menu</h6>
            <a class="collapse-item" href="<?=base_url('nilai');?>">Input/Lihat Nilai</a>
            <a class="collapse-item" href="<?=base_url('nilai/persiswa');?>">Daftar Nilai Per Siswa</a>
          </div>
        </div>
      </li>

    <li class="nav-item <?php if($this->uri->segment(2)=='siswa' || $this->uri->segment(2)=='guru' || $this->uri->segment(2)=='admin'){echo 'active';}?>">
      <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#data_user"
      aria-expanded="true" aria-controls="data_user">
      <i class="fa fa-users"></i>
      <span>Data User</span>
    </a>
    <div id="data_user" class="collapse" aria-labelledby="headingBootstrap" data-parent="#accordionSidebar">
      <div class="bg-white py-2 collapse-inner rounded">
        <h6 class="collapse-header">Pilih User</h6>
        <a class="collapse-item <?php if($this->uri->segment(2)=='guru'){echo 'active';}?>" href="<?=base_url('users/guru');?>">Guru</a>
        <a class="collapse-item <?php if($this->uri->segment(2)=='siswa'){echo 'active';}?>" href="<?=base_url('users/siswa');?>">Siswa</a>
      </div>
    </div>
  </li>

  <li class="nav-item">
    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#data_akademik"
    aria-expanded="true" aria-controls="data_akademik">
    <i class="fa fa-briefcase"></i>
    <span>Data Akademik</span>
  </a>
  <div id="data_akademik" class="collapse" aria-labelledby="headingBootstrap" data-parent="#accordionSidebar">
    <div class="bg-white py-2 collapse-inner rounded">
      <h6 class="collapse-header">Pilih Akademik</h6>
      <a class="collapse-item" href="<?=base_url('akademik/jurusan');?>">Jurusan</a>
      <a class="collapse-item" href="<?=base_url('akademik/kelas');?>">Kelas</a>
      <a class="collapse-item" href="<?=base_url('akademik/pelajaran');?>">Mata Pelajaran</a>
      <a class="collapse-item" href="<?=base_url('akademik/tahun_ajaran');?>">Tahun Ajaran</a>
    </div>
  </div>
</li>



<li class="nav-item">
  <a class="nav-link" href="javascript:void(0);" data-toggle="modal" data-target="#logoutModal">
    <i class="fas fa-fw fa-sign-out-alt"></i>
    <span>Keluar</span>
  </a>
</li>

<!-- SISWA -->
<?php }else if($this->session->userdata('sess_role') == 'siswa'){ ?>
  <li class="nav-item <?php if($this->uri->segment(1)=='home' || $this->uri->segment(1)==''){echo 'active';}?>">
    <a class="nav-link" href="<?=base_url('.');?>">
      <i class="fas fa-fw fa-tachometer-alt"></i>
      <span>Dashboard</span></a>
    </li>

    <hr class="sidebar-divider">
    <div class="sidebar-heading">
      Select Menu
    </div>


    <li class="nav-item <?php if($this->uri->segment(1)=='nilai'){echo 'active';}?>">
      <a class="nav-link" href="<?=base_url('nilai/check');?>">
        <i class="fas fa-fw fa-pen-square"></i>
        <span>Nilai</span>
      </a>
    </li>

    <li class="nav-item">
      <a class="nav-link" href="javascript:void(0);" data-toggle="modal" data-target="#logoutModal">
        <i class="fas fa-fw fa-sign-out-alt"></i>
        <span>Keluar</span>
      </a>
    </li>

    <!-- PUBLIK -->
  <?php }else{ ?> 
    <li class="nav-item <?php if($this->uri->segment(1)=='home' || $this->uri->segment(1)==''){echo 'active';}?>">
      <a class="nav-link" href="<?=base_url('.');?>">
        <i class="fas fa-fw fa-tachometer-alt"></i>
        <span>Dashboard</span></a>
      </li>

<!--         <li class="nav-item <?php if($this->uri->segment(1)=='artikel'){echo 'active';}?>">
          <a class="nav-link" href="<?=base_url('artikel');?>">
            <i class="fas fa-fw fa-newspaper"></i>
            <span>Artikel</span>
          </a>
        </li> -->

        <li class="nav-item <?php if($this->uri->segment(1)=='tentang'){echo 'active';}?>">
          <a class="nav-link" href="<?=base_url('tentang');?>">
            <i class="fas fa-fw fa-info-circle"></i>
            <span>Tentang</span>
          </a>
        </li>

        <li class="nav-item">
          <a class="nav-link" href="<?=base_url('auth/login');?>">
            <i class="fas fa-fw fa-sign-in-alt"></i>
            <span>Masuk</span>
          </a>
        </li>
      <?php } ?>

    </ul>