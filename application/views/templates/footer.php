<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabelLogout"
aria-hidden="true">
<div class="modal-dialog" role="document">
  <div class="modal-content">
    <div class="modal-header bg-gradient-primary text-white">
      <h5 class="modal-title" id="exampleModalLabelLogout">Ohh Tidak!</h5>
      <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
      <p>Apakah anda yakin akan keluar?</p>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Batal</button>

      <a href="<?=base_url('auth/logout');?>" class="btn btn-primary btn-icon-split"><span class="icon text-white-50">
        <i class="fas fa-sign-out-alt"></i>
      </span>
      <span class="text">Keluar</span>
    </a>
  </div>
</div>
</div>
</div>

<div class="modal fade" id="ubahPasswordModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabelLogout"
aria-hidden="true">
<div class="modal-dialog" role="document">
  <div class="modal-content">
    <div class="modal-header bg-gradient-primary text-white">
      <h5 class="modal-title" id="exampleModalLabelLogout">Ubah Password Anda</h5>
      <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
      <form id="ubah_password_form" method="POST" action="<?=base_url('users/ubah_password');?>">
        <input type="hidden" name="id_users_pw" value="<?php echo $this->session->userdata('sess_id'); ?>">
        <div class="form-group">
          <label for="old_password">Password Lama</label>
          <div class="input-group">
            <input type="password" name="old_password" id="old_password" class="form-control" data-toggle="password" required="required">
            <div class="input-group-append">
              <span class="input-group-text">
                <i class="fa fa-eye"></i>
              </span>
            </div>
          </div>
        </div>
        <div class="form-group">
          <label for="password">Password Baru</label>
          <div class="input-group">
            <input type="password" name="password" id="password" class="form-control" data-toggle="password" required="required">
            <div class="input-group-append">
              <span class="input-group-text">
                <i class="fa fa-eye"></i>
              </span>
            </div>
          </div>
        </div>
        <div class="form-group">
          <label for="repeat_password">Konfirmasi Password Baru</label>
          <div class="input-group">
            <input type="password" name="repeat_password" id="repeat_password" class="form-control" data-toggle="password" required="required">
            <div class="input-group-append">
              <span class="input-group-text">
                <i class="fa fa-eye"></i>
              </span>
            </div>
          </div>
        </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Batal</button>
        <button type="button" class="btn btn-primary btn-icon-split" id="btn_ubah_password">
          <span class="icon text-white-50">
            <i class="fas fa-save"></i>
          </span>
          <span class="text">Simpan Perubahan</span>
        </button>
      </form>
    </div>
  </div>
</div>
</div>

</div>
</div>

<!-- Footer -->
<footer class="sticky-footer bg-white">
 <div class="container my-auto">
  <div class="copyright text-center my-auto">
   <span>copyright &copy; <script> document.write(new Date().getFullYear()); </script></b>
   </span>
 </div>
</div>
</footer>
<!-- Footer -->
</div>
</div>

<!-- Scroll to top -->
<a class="scroll-to-top rounded" href="#page-top">
	<i class="fas fa-angle-up"></i>
</a>

<div class="toast" id="myToast" style="position: absolute; top: 80px; right: 0px;" data-delay="10000">

</div>
<script src="<?=base_url('assets/js/myscript.min.js');?>"></script>
<script src="<?=base_url('assets/js/bootstrap-show-password.min.js');?>"></script>
  <!-- Page level custom scripts -->
<script>
  var flashdata = "<?php echo $this->session->flashdata('msg') ?>";
  

  create_toast();

  function create_toast(){
    if (flashdata !== '') {
      if (flashdata == 'success') {
        var notif = '<strong class="mr-auto"><i class="fa fa-check"></i> PERMINTAAN BERHASIL</strong>';
        var change_color_toas = 'bg-gradient-success';
        var text_toast = 'Data baru telah berhasil ditambahkan!';
      }else if (flashdata == 'failed') {
        var notif = '<strong class="mr-auto"><i class="fa fa-times"></i> PERMINTAAN GAGAL</strong>';
        var change_color_toas = 'bg-gradient-danger';
        var text_toast = 'Data baru telah gagal ditambahkan!';
      }else if (flashdata == 'delete') {
        var notif = '<strong class="mr-auto"><i class="fa fa-check"></i> PERMINTAAN BERHASIL</strong>';
        var change_color_toas = 'bg-gradient-success';
        var text_toast = 'Data telah berhasil dihapus!';
      }else if (flashdata == 'update') {
        var notif = '<strong class="mr-auto"><i class="fa fa-check"></i> PERMINTAAN BERHASIL</strong>';
        var change_color_toas = 'bg-gradient-success';
        var text_toast = 'Data telah berhasil perbaharui!';
      }else if (flashdata == 'ubah_password') {
        var notif = '<strong class="mr-auto"><i class="fa fa-check"></i> PERMINTAAN BERHASIL</strong>';
        var change_color_toas = 'bg-gradient-success';
        var text_toast = 'Password telah berhasil diubah!';
      }else if (flashdata == 'ubah_password_fail') {
        var notif = '<strong class="mr-auto"><i class="fa fa-times"></i> PERMINTAAN GAGAL</strong>';
        var change_color_toas = 'bg-gradient-danger';
        var text_toast = 'Password telah gagal diubah!';
      }else if (flashdata == 'update_fail') {
        var notif = '<strong class="mr-auto"><i class="fa fa-times"></i> PERMINTAAN GAGAL</strong>';
        var change_color_toas = 'bg-gradient-danger';
        var text_toast = 'Data telah gagal perbaharui!';
      }else if (flashdata == 'nilai_success') {
        var notif = '<strong class="mr-auto"><i class="fa fa-check"></i> PERMINTAAN BERHASIL</strong>';
        var change_color_toas = 'bg-gradient-success';
        var text_toast = 'Penilaian telah berhasil tercatat oleh sistem!';
      }else if (flashdata == 'nilai_fail') {
        var notif = '<strong class="mr-auto"><i class="fa fa-times"></i> PERMINTAAN GAGAL</strong>';
        var change_color_toas = 'bg-gradient-danger';
        var text_toast = 'Penilaian telah gagal tercatat oleh sistem! Data sudah ada.';
      }
      else if (flashdata == 'no_data') {
        var notif = '<strong class="mr-auto"><i class="fa fa-times"></i> PERMINTAAN GAGAL</strong>';
        var change_color_toas = 'bg-gradient-danger';
        var text_toast = 'Tidak ada data siswa pada opsi yang anda pilih! atau data sudah diinput.';
      }

      $('#myToast').html(`<div class="toast-header `+change_color_toas+` text-white">
        `+notif+`
        <button type="button" class="ml-2 mb-1 close" data-dismiss="toast">
        <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="toast-body">
        <div>`+text_toast+`</div>
        </div>`);

      $("#myToast").toast('show');
    }
    

  }
  $('#btn_ubah_password').click(function(){
    if ($('#old_password').val()=='') {
      alert('Password lama tidak boleh kosong!');
    }else if($('#password').val()==''){
      alert('Password Baru tidak boleh kosong!');
    }else if($('#repeat_password').val()==''){
      alert('Konfirmasi password baru tidak boleh kosong!');
    }else if($('#password').val()!=$('#repeat_password').val()){
      alert('Password tidak cocok!');
    }else{
      $('#ubah_password_form').submit();
    }
  })
</script>
</body>

</html>