  <div id="content-wrapper" class="d-flex flex-column">
    <div id="content">
      <!-- TopBar -->
      <nav class="navbar navbar-expand navbar-light bg-gradient-primary topbar mb-4 static-top">
        <button id="sidebarToggleTop" class="btn btn-link rounded-circle mr-3">
          <i class="fa fa-bars"></i>
        </button>
        <ul class="navbar-nav ml-auto">
          <?php if($this->session->userdata('sess_role') !== null){ ?> 
            <li class="nav-item dropdown no-arrow">
              <a class="nav-link" href="javascript:void(0);" data-toggle="modal" data-target="#ubahPasswordModal">
                <i class="fas fa-key"></i> Ubah Password
              </a>
            </li>

            <div class="topbar-divider d-sm-block"></div>
            <li class="nav-item dropdown no-arrow">
              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown"
              aria-haspopup="true" aria-expanded="false">
              <img class="img-profile rounded-circle" src="<?=base_url('assets/img/boy.png');?>" style="max-width: 60px">
              <span class="ml-2 d-none d-lg-inline text-white small"><?php echo $this->session->userdata('sess_nama');?></span>
            </a>
            <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
              <a class="dropdown-item" href="javascript:void(0);" data-toggle="modal" data-target="#logoutModal">
                <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                Keluar
              </a>
            </div>
          </li>
        <?php } ?>

      </ul>
    </nav>
    <!-- Topbar -->
    <div class="container-fluid" id="container-wrapper">
