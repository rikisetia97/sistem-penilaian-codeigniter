<?php $this->load->view('templates/header') ?>
<?php $this->load->view('templates/sidebar') ?>
<?php $this->load->view('templates/navbar') ?>

<!-- BreadCumb-->
<div class="d-sm-flex align-items-center justify-content-between">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?=base_url('.');?>"><i class="fa fa-home"></i> Dashboard</a></li>
    <li class="breadcrumb-item active">Nilai</li>
  </ol>
</div>

<div class="row mb-3">

  <div class="col-lg-6">
    <div class="card mb-4">
      <div class="card-header bg-gradient-primary py-3 d-flex flex-row align-items-center justify-content-between">
        <h6 class="m-0 text-white"><i class="fa fa-pen-square"></i> Input Nilai Baru</h6>
      </div>
      <div class="p-3">
        <form method="POST" action="<?=base_url('nilai/input');?>">
          <div class="form-group row">
            <label class="col-sm-3 col-form-label">Jurusan</label>
            <div class="col-sm-9">
             <select class="select1 form-control" name="id_jurusan" required="required" style="width: 100%">
              <option value="">Select</option>
              <?php foreach ($jurusan as $key) { ?> 
                <option value="<?=$key->id;?>/nxa/<?=$key->jurusan;?>"><?=$key->jurusan;?></option>
              <?php } ?>
            </select>
          </div>
        </div>
        <div class="form-group row">
          <label class="col-sm-3 col-form-label">Kelas</label>
          <div class="col-sm-9">
           <select class="form-control select6" name="id_kelas" required="required" style="width: 100%">
            <option value="">Select</option>
            <?php foreach ($kelas as $key) { ?> 
              <option value="<?=$key->id;?>/nxa/<?=$key->kelas;?>"><?=$key->kelas;?></option>
            <?php } ?>
          </select>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-sm-3 col-form-label">Mata Pelajaran</label>
        <div class="col-sm-9">
         <select class="form-control select3" name="id_pelajaran" required="required" style="width: 100%">
          <option value="">Select</option>
          <?php foreach ($pelajaran as $key) { ?> 
            <option value="<?=$key->id;?>/nxa/<?=$key->mata_pelajaran;?>"><?=$key->mata_pelajaran;?></option>
          <?php } ?>
        </select>
      </div>
    </div>
    <div class="form-group row">
      <label class="col-sm-3 col-form-label">Semester</label>
      <div class="col-sm-9">
       <select class="form-control select4" name="id_semester" required="required" style="width: 100%">
        <option value="">Select</option>
        <?php foreach ($semester as $key) { ?> 
          <option value="<?=$key->id;?>/nxa/<?=$key->semester;?>"><?=$key->semester;?></option>
        <?php } ?>
      </select>
    </div>
  </div>
  <div class="form-group row">
    <label class="col-sm-3 col-form-label">Tahun Ajaran</label>
    <div class="col-sm-9">
     <select class="form-control select5" name="id_tahun_ajaran" required="required" style="width: 100%">
      <option value="">Select</option>
      <?php foreach ($tahun_ajaran as $key) { ?> 
        <option value="<?=$key->id;?>/nxa/<?=$key->tahun_ajaran;?>"><?=$key->tahun_ajaran;?></option>
      <?php } ?>
    </select>
  </div>
</div>

<div class="form-group row">
  <label class="col-sm-3 col-form-label">Nilai KKM</label>
  <div class="col-sm-9">
    <input type="number" class="form-control" name="kkm" required="required" placeholder="Nilai KKM">
  </div>
</div>

<button type="submit" class="btn btn-primary btn-icon-split">
  <span class="icon text-white-50">
    <i class="fas fa-paper-plane"></i>
  </span>
  <span class="text">Lanjut</span>
</button>
</form>
</div>
</div>
</div>
<div class="col-lg-6">
  <div class="card mb-4">
    <div class="card-header bg-gradient-primary py-3 d-flex flex-row align-items-center justify-content-between">
      <h6 class="m-0 text-white"><i class="fa fa-pen-square"></i> Tampilkan Nilai Siswa</h6>
    </div>
    <div class="p-3">
     <div class="form-group row">
      <label class="col-sm-3 col-form-label">Jurusan</label>
      <div class="col-sm-9">
       <select class="select1 form-control id_jurusan" required="required" style="width: 100%">
        <option value="">Select</option>
        <?php foreach ($jurusan as $key) { ?> 
          <option value="<?=$key->id;?>"><?=$key->jurusan;?></option>
        <?php } ?>
      </select>
    </div>
  </div>
  <div class="form-group row">
    <label class="col-sm-3 col-form-label">Kelas</label>
    <div class="col-sm-9">
     <select class="form-control select6 id_kelas" required="required" style="width: 100%">
      <option value="">Select</option>
      <?php foreach ($kelas as $key) { ?> 
        <option value="<?=$key->id;?>"><?=$key->kelas;?></option>
      <?php } ?>
    </select>
  </div>
</div>
<div class="form-group row">
  <label class="col-sm-3 col-form-label">Mata Pelajaran</label>
  <div class="col-sm-9">
   <select class="form-control select3 id_pelajaran" required="required" style="width: 100%">
    <option value="">Select</option>
    <?php foreach ($pelajaran as $key) { ?> 
      <option value="<?=$key->id;?>"><?=$key->mata_pelajaran;?></option>
    <?php } ?>
  </select>
</div>
</div>
<div class="form-group row">
  <label class="col-sm-3 col-form-label">Semester</label>
  <div class="col-sm-9">
    <select class="form-control select4 id_semester" required="required" style="width: 100%">
      <option value="">Select</option>
      <?php foreach ($semester as $key) { ?> 
        <option value="<?=$key->id;?>"><?=$key->semester;?></option>
      <?php } ?>
    </select>
  </div>
</div>
<div class="form-group row">
  <label class="col-sm-3 col-form-label">Tahun Ajaran</label>
  <div class="col-sm-9">
   <select class="form-control select5 id_tahun_ajaran" required="required" style="width: 100%">
    <option value="">Select</option>
    <?php foreach ($tahun_ajaran as $key) { ?> 
      <option value="<?=$key->id;?>"><?=$key->tahun_ajaran;?></option>
    <?php } ?>
  </select>
</div>
</div>

<button type="button" onclick="btn_tampil()" class="btn btn-primary btn-icon-split">
  <span class="icon text-white-50">
    <i class="fas fa-eye"></i>
  </span>
  <span class="text">Tampilkan</span>
</button>
</div>
</div>
</div>
</div>

<script>
  var base_url = "<?php echo base_url() ?>";
  $(document).ready(function () {
    $('.select1').select2({
      placeholder: "Pilih Jurusan",
      allowClear: true
    });
    $('.select6').select2({
      placeholder: "Pilih Kelas",
      allowClear: true
    });
    $('.select3').select2({
      placeholder: "Pilih Mata Pelajaran",
      allowClear: true
    });
    $('.select4').select2({
      placeholder: "Pilih Semester",
      allowClear: true
    });
    $('.select5').select2({
      placeholder: "Pilih Tahun Ajaran",
      allowClear: true
    });
    $('#dataTableHover').DataTable();
  });

  function btn_tampil() {
    var id_jurusan = $('.id_jurusan').val();
    var id_kelas = $('.id_kelas').val();
    var id_pelajaran = $('.id_pelajaran').val();
    var id_semester = $('.id_semester').val();
    var id_tahun_ajaran = $('.id_tahun_ajaran').val();

    if ($('.id_jurusan').val()=='') {
      alert('Jurusan tidak boleh kosong!');
    }else if($('.id_kelas').val()==''){
      alert('Kelas tidak boleh kosong!');
    }else if($('.id_pelajaran').val()==''){
      alert('Mata pelajaran tidak boleh kosong!');
    }else if($('.id_semester').val()==''){
      alert('Semester tidak boleh kosong!');
    }else if($('.id_tahun_ajaran').val()==''){
      alert('Tahun ajaran tidak boleh kosong!');
    }else{
      var url = btoa(id_jurusan+'|'+id_kelas+'|'+id_pelajaran+'|'+id_semester+'|'+id_tahun_ajaran);
      console.log(base_url+'nilai/daftar/'+url);
      window.location = base_url+"nilai/daftar/"+url;
    }
  }

</script>
<?php $this->load->view('templates/footer') ?>
