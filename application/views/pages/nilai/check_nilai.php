<?php $this->load->view('templates/header') ?>
<?php $this->load->view('templates/sidebar') ?>
<?php $this->load->view('templates/navbar') ?>

<!-- BreadCumb-->
<div class="d-sm-flex align-items-center justify-content-between row">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?=base_url('.');?>"><i class="fa fa-home"></i> Dashboard</a></li>
    <li class="breadcrumb-item">Nilai</li>
    <li class="breadcrumb-item active">Daftar</li>
  </ol>
  <div class="col-lg-4" align="right">
    <a href="#" class="btn btn-success btn-icon-split mb-2" onclick="cetak_laporan()">
      <span class="icon text-white-50">
        <i class="fas fa-print"></i>
      </span>
      <span class="text">Cetak</span>
    </a>
    <button class="btn btn-primary dropdown-toggle btn-icon-split mb-2" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      <span class="icon text-white-50">
        <i class="fas fa-caret-down"></i>
      </span>
      <span class="text">Pilih Semester</span>
    </button>

    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 38px, 0px);">
      <?php foreach($get_semester as $key){ ?>
        <a class="dropdown-item" href="<?=base_url('nilai/check/'.$key->id);?>"><?=$key->semester;?></a>
      <?php } ?>
    </div>
  </div>
</div>

<div class="row mb-3">

  <div class="col-lg-12">
    <div class="card mb-4">
      <div class="card-header bg-gradient-primary py-3 d-flex flex-row align-items-center justify-content-between">
        <h6 class="m-0 text-white"><i class="fa fa-pen-square"></i> Daftar Nilai Akhir Siswa</h6>
      </div>
      <div class="row">
        <div class="p-3 col-lg-6">
          <div class="table-responsive">

            <table align="center">
              <tr>
                <td width="200">Nama Siswa</td>
                <td>:</td>
                <td width="200" align="right"><?=$nama;?></td>
              </tr>
              <tr>
                <td width="200">Jurusan</td>
                <td>:</td>
                <td width="200" align="right"><?=$jurusan;?></td>
              </tr>
              <tr>
                <td width="200">Kelas</td>
                <td>:</td>
                <td width="200" align="right"><?=$kelas;?></td>
              </tr>
            </table>
          </div>
        </div>
        <div class="p-3 col-lg-6">
          <div class="table-responsive">
            <table align="center">
              <tr>
                <td width="200">Semester</td>
                <td>:</td>
                <td width="200" align="right"><?=$semester;?></td>
              </tr>
              <tr>
                <td width="200">Tahun Ajaran</td>
                <td>:</td>
                <td width="200" align="right"><?=$tahun_ajaran;?></td>
              </tr>
            </table>
          </div>
        </div>
        <div class="col-lg-12">
          <!-- Simple Tables -->
          <div class="card">
            <div class="table-responsive">
              <table class="table align-items-center table-hover table-flush">
                <thead class="thead-light">
                  <tr>
                    <th>No.</th>
                    <th>Mata Pelajaran</th>
                    <th>Kelas</th>
                    <th>Tahun Ajaran</th>
                    <th>Nilai KKM</th>
                    <th>Nilai</th>
                  </tr>
                  <tfoot class="thead-light">
                    <tr>
                      <th>No.</th>
                      <th>Mata Pelajaran</th>
                      <th>Kelas</th>
                      <th>Tahun Ajaran</th>
                      <th>Nilai KKM</th>
                      <th>Nilai</th>
                    </tr>
                  </tfoot>
                </thead>
                <tbody>
                  <?php $i=1; foreach ($user as $key) {  ?>
                    <tr>
                      <td width="20"><?=$i++;?></td>
                      <td><?=$key['pelajaran'];?></td>
                      <td width="200"><?=$key['kelas_nilai'];?></td>
                      <td width="200"><?=$key['tahun_ajaran_nilai'];?></td>
                      <td width="200"><?=$key['nilai']->kkm;?></td>
                      <td width="200"><?=$key['nilai']->nilai;?></td>

                    </tr>
                  <?php } ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Modal Edit -->
<div class="modal fade" id="edit_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content">

     <div class="modal-header bg-gradient-primary text-white">
      <h5 class="modal-title" id="exampleModalScrollableTitle">Ubah Nilai</h5>
      <button type="button" class="close  text-white" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
      <form method="POST" action="<?=base_url('nilai/update');?>">
        <input type="hidden" id="ed_id" name="ed_id">
        <div class="form-group">
          <label for="ed_nilai">Nilai</label>
          <input type="text" class="form-control" id="ed_nilai" name="ed_nilai" required="required">
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Tutup</button>
        <button type="submit" class="btn btn-primary btn-icon-split">
          <span class="icon text-white-50">
            <i class="fas fa-save"></i>
          </span>
          <span class="text">Simpan Perubahan</span>
        </button>
      </form>
    </div>
  </div>
</div>
</div>
<script>
  var base_url = "<?php echo base_url() ?>";
  var url = "<?php echo $this->uri->segment(3) ?>";
  function cetak_laporan() {
    window.location = base_url+'export/nilai_by_siswa/'+url;
  }

  function edit_data(id) {
    $.ajax({
      url: base_url+"nilai/detail/"+id, 
      type: "GET",
      success: function(result){
        var obj = JSON.parse(result);

        $('#ed_id').val(obj['id']);
        $('#ed_nilai').val(obj['nilai']);
        $('#edit_modal').modal('show');
      }
    });
  }

</script>
<?php $this->load->view('templates/footer') ?>
