<?php $this->load->view('templates/header') ?>
<?php $this->load->view('templates/sidebar') ?>
<?php $this->load->view('templates/navbar') ?>

<!-- BreadCumb-->
<div class="d-sm-flex align-items-center justify-content-between row">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?=base_url('.');?>"><i class="fa fa-home"></i> Dashboard</a></li>
    <li class="breadcrumb-item">Nilai</li>
    <li class="breadcrumb-item active">Daftar</li>
  </ol>
  <div class="col-lg-4" align="right">

    <a href="javascript:void(0)" onclick="cetak_laporan_pdf()" class="btn btn-primary btn-icon-split mb-2">
      <span class="icon text-white-50">
        <i class="fas fa-print"></i>
      </span>
      <span class="text">Cetak PDF</span>
    </a>
  </div>
</div>
<div class="row mb-3">

  <div class="col-lg-12">
    <div class="card mb-4">
      <div class="card-header bg-gradient-primary py-3 d-flex flex-row align-items-center justify-content-between">
        <h6 class="m-0 text-white"><i class="fa fa-pen-square"></i> Cari Nilai Siswa</h6>
      </div>
      <div class="p-3">
        <div class="form-group row">
          <label class="col-sm-3 col-form-label">Pilih Kelas</label>
          <div class="col-sm-9">
           <select class="form-control select5" name="id_kelas" id="id_kelas" required="required" style="width: 100%">
            <option value="">Select</option>
            <?php foreach ($kelas as $key) { ?> 
              <option value="<?=$key->id;?>"><?=$key->kelas;?></option>
            <?php } ?>
          </select>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-sm-3 col-form-label">Pilih Siswa</label>
        <div class="col-sm-9">
         <select class="form-control select6" name="id_siswa" id="id_siswa" required="required" style="width: 100%">
          <option value="">Select</option>
          <?php foreach ($siswa as $key) { ?> 
            <option value="<?=$key->id;?>"><?=$key->nama;?></option>
          <?php } ?>
        </select>
      </div>
    </div>
    <div class="form-group row">
      <label class="col-sm-3 col-form-label">Pilih Semester</label>
      <div class="col-sm-9">
       <select class="form-control select3" name="id_semester" id="id_semester" required="required" style="width: 100%">
        <option value="">Select</option>
        <?php foreach ($semester as $key) { ?> 
          <option value="<?=$key->id;?>"><?=$key->semester;?></option>
        <?php } ?>
      </select>
    </div>
  </div>
  <div class="form-group row">
    <label class="col-sm-3 col-form-label">Pilih Tahun Ajaran</label>
    <div class="col-sm-9">
     <select class="form-control select4" name="id_tahun_ajaran" id="id_tahun_ajaran" required="required" style="width: 100%">
      <option value="">Select</option>
      <?php foreach ($tahun_ajaran as $key) { ?> 
        <option value="<?=$key->id;?>"><?=$key->tahun_ajaran;?></option>
      <?php } ?>
    </select>
  </div>
</div>
<button type="button" class="btn btn-primary btn-icon-split btn_search">
  <span class="icon text-white-50">
    <i class="fas fa-search"></i>
  </span>
  <span class="text">Cari</span>
</button>
</div>
</div>
</div>
</div>
<div class="row mb-3">

  <div class="col-lg-12" id="show_table" style="display: none;">
    <div class="card mb-4">
      <div class="card-header bg-gradient-primary py-3 d-flex flex-row align-items-center justify-content-between">
        <h6 class="m-0 text-white"><i class="fa fa-pen-square"></i> Daftar Nilai Siswa</h6>
      </div>
      <div class="row">
        <div class="p-3 col-lg-6">
          <div class="table-responsive">

            <table align="center">
              <tr>
                <td width="200">Nama</td>
                <td>:</td>
                <td width="200" align="right" id="nama_siswa">Test Siswa</td>
              </tr>
              <tr>
                <td width="200">NIS / NISN:</td>
                <td>:</td>
                <td width="200" align="right" id="no_induk">123444</td>
              </tr>
            </table>
          </div>
        </div>
        <div class="p-3 col-lg-6">
          <div class="table-responsive">
            <table align="center">
              <tr>
                <td width="200">Kelas</td>
                <td>:</td>
                <td width="200" align="right" id="kelas">XI TKJ 1</td>
              </tr>
              <tr>
                <td width="200">Semester / Tahun Ajaran</td>
                <td>:</td>
                <td width="200" align="right" id="semester">1 / 2021-2022</td>
              </tr>
            </table>
          </div>
        </div>
        <div class="col-lg-12">
          <!-- Simple Tables -->
          <div class="card">
            <div class="table-responsive">
              <table class="table align-items-center table-hover table-flush">
                <thead class="thead-light">
                  <tr>
                    <th>No.</th>
                    <th>Mata Pelajaran</th>
                    <th>Nilai KKM</th>
                    <th>Nilai</th>
                    <th>Predikat</th>
                  </tr>
                  <tfoot class="thead-light">
                    <tr>
                      <th>No.</th>
                      <th>Mata Pelajaran</th>
                      <th>Nilai KKM</th>
                      <th>Nilai</th>
                      <th>Predikat</th>
                    </tr>
                  </tfoot>
                </thead>
                <tbody id="list_data">

                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Modal Edit -->
<div class="modal fade" id="edit_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content">

     <div class="modal-header bg-gradient-primary text-white">
      <h5 class="modal-title" id="exampleModalScrollableTitle">Ubah Nilai</h5>
      <button type="button" class="close  text-white" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
      <form method="POST" action="<?=base_url('nilai/update');?>">
        <input type="hidden" id="ed_id" name="ed_id">
        <div class="form-group">
          <label for="ed_nilai">Nilai</label>
          <input type="text" class="form-control" id="ed_nilai" name="ed_nilai" required="required">
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Tutup</button>
        <button type="submit" class="btn btn-primary btn-icon-split">
          <span class="icon text-white-50">
            <i class="fas fa-save"></i>
          </span>
          <span class="text">Simpan Perubahan</span>
        </button>
      </form>
    </div>
  </div>
</div>
</div>
<script>
  var base_url = "<?php echo base_url() ?>";
  var url = "<?php echo $this->uri->segment(3) ?>";
  $('.select3').select2({
    placeholder: "Pilih Semester",
    allowClear: true
  });  
  $('.select4').select2({
    placeholder: "Pilih Tahun Ajaran",
    allowClear: true
  });  
  $('.select5').select2({
    placeholder: "Pilih Kelas",
    allowClear: true
  });  
  $('.select6').select2({
    placeholder: "Pilih Siswa",
    allowClear: true
  });

  function cetak_laporan_pdf() {
    id_kelas = $('#id_kelas').val();
    id_semester = $('#id_semester').val();
    id_users = $('#id_siswa').val();
    id_tahun_ajaran = $('#id_tahun_ajaran').val();
    window.location = base_url+'export/nilai_persiswa/'+id_kelas+'/'+id_semester+'/'+id_users+'/'+id_tahun_ajaran+'/';
  }

  $('.btn_search').click(function(){
    id_kelas = $('#id_kelas').val();
    id_siswa = $('#id_siswa').val();
    id_semester = $('#id_semester').val();
    id_tahun_ajaran = $('#id_tahun_ajaran').val();
    if (id_kelas != '' || id_siswa != '' || id_semester != ''|| id_tahun_ajaran != '') {
      $.ajax({
        url: base_url+"nilai/get_nilai_all_mapel/"+id_kelas+"/"+id_semester+"/"+id_siswa+"/"+id_tahun_ajaran, 
        type: "GET",
        success: function(result){
          var obj = JSON.parse(result);
          $('#nama_siswa').html(obj.nama_siswa);
          $('#no_induk').html(obj.no_induk);
          $('#semester').html(obj.semester+' / '+obj.tahun_ajaran);
          $('#list_data').html('');
          a = 1;
          for (var i = obj['nilai'].length - 1; i >= 0; i--) {
           console.log( )
           $('#list_data').append(`
            <tr>
            <td width="20">`+ a++ +`</td>
            <td>`+ obj['nilai'][i].pelajaran +`</td>
            <td width="200">`+obj['nilai'][i].kkm+`</td>
            <td width="200">`+obj['nilai'][i].nilai+`</td>
            <td width="200">`+obj['nilai'][i].predikat+`</td>
            </tr>
            `);
         }
         $('#show_table').removeAttr('style');
       }
     });
    }
  })

</script>
<?php $this->load->view('templates/footer') ?>
