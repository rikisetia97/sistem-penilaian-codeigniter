<?php $this->load->view('templates/header') ?>
<?php $this->load->view('templates/sidebar') ?>
<?php $this->load->view('templates/navbar') ?>

<!-- BreadCumb-->
<div class="d-sm-flex align-items-center justify-content-between row">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?=base_url('.');?>"><i class="fa fa-home"></i> Dashboard</a></li>
    <li class="breadcrumb-item">Nilai</li>
    <li class="breadcrumb-item active">Input</li>
  </ol>
  <div class="col-lg-4" align="right">

    <a href="javascript:void(0)" onclick="$('#form_penilaian').submit();" class="btn btn-primary btn-icon-split mb-2">
      <span class="icon text-white-50">
        <i class="fas fa-save"></i>
      </span>
      <span class="text">Simpan Nilai</span>
    </a>
  </div>
</div>

<div class="row mb-3">

  <div class="col-lg-12">
    <div class="card mb-4">
      <div class="card-header bg-gradient-primary py-3 d-flex flex-row align-items-center justify-content-between">
        <h6 class="m-0 text-white"><i class="fa fa-pen-square"></i> Prosess Input Nilai</h6>
      </div>
      <div class="row">
        <div class="p-3 col-lg-6">
          <div class="table-responsive">

            <table align="center">
              <tr>
                <td width="200">Mata Pelajaran</td>
                <td>:</td>
                <td width="200" align="right"><?=$pelajaran;?></td>
              </tr>
              <tr>
                <td width="200">Jurusan</td>
                <td>:</td>
                <td width="200" align="right"><?=$jurusan;?></td>
              </tr>
              <tr>
                <td width="200">Kelas</td>
                <td>:</td>
                <td width="200" align="right"><?=$kelas;?></td>
              </tr>
            </table>
          </div>
        </div>
        <div class="p-3 col-lg-6">
          <div class="table-responsive">
            <table align="center">
              <tr>
                <td width="200">Semester</td>
                <td>:</td>
                <td width="200" align="right"><?=$semester;?></td>
              </tr>
              <tr>
                <td width="200">Tahun Ajaran</td>
                <td>:</td>
                <td width="200" align="right"><?=$tahun_ajaran;?></td>
              </tr>
            </table>
          </div>
        </div>
        <div class="col-lg-12">
          <!-- Simple Tables -->
          <div class="card">
            <div class="table-responsive">
              <table class="table align-items-center table-hover table-flush">
                <thead class="thead-light">
                  <tr>
                    <th>No.</th>
                    <th>Nama Siswa</th>
                    <th>Nilai KKM</th>
                    <th>Nilai</th>
                  </tr>
                  <tfoot class="thead-light">
                    <tr>
                      <th>No.</th>
                      <th>Nama Siswa</th>
                      <th>Nilai KKM</th>
                      <th>Nilai</th>
                    </tr>
                  </tfoot>
                </thead>
                <tbody>
                  <form id="form_penilaian" method="POST" action="<?=base_url('nilai/simpan_nilai');?>">
                    <input type="hidden" name="id_jurusan" value="<?=$id_jurusan;?>">
                    <input type="hidden" name="jurusan" value="<?=$jurusan;?>">
                    <input type="hidden" name="id_kelas" value="<?=$id_kelas;?>">
                    <input type="hidden" name="kelas" value="<?=$kelas;?>">
                    <input type="hidden" name="id_pelajaran" value="<?=$id_pelajaran;?>">
                    <input type="hidden" name="pelajaran" value="<?=$pelajaran;?>">
                    <input type="hidden" name="id_semester" value="<?=$id_semester;?>">
                    <input type="hidden" name="semester" value="<?=$semester;?>">
                    <input type="hidden" name="id_tahun_ajaran" value="<?=$id_tahun_ajaran;?>">
                    <input type="hidden" name="tahun_ajaran" value="<?=$tahun_ajaran;?>">
                    <input type="hidden" name="kkm" value="<?=$kkm;?>">
                    <?php $i=1; foreach ($siswa as $key) { ?>
                      <input type="hidden" name="id_users[]" value="<?=$key->id;?>">
                      <tr>
                        <td width="20"><?=$i++;?></td>
                        <td><?=$key->nama;?></td>
                        <td width="200"><?=$kkm;?></td>
                        <td width="180">
                          <input type="number" class="form-control" name="nilai[]" style="width: 70px">
                        </td>
                      </tr>
                    <?php } ?>
                  </form>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<?php $this->load->view('templates/footer') ?>
