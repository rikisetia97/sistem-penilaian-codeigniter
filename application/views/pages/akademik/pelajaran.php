<?php $this->load->view('templates/header') ?>
<?php $this->load->view('templates/sidebar') ?>
<?php $this->load->view('templates/navbar') ?>

<!-- BreadCumb-->
<div class="d-sm-flex align-items-center justify-content-between">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?= base_url('.'); ?>"><i class="fa fa-home"></i> Dashboard</a></li>
    <li class="breadcrumb-item">Akademik</li>
    <li class="breadcrumb-item active">Mata Pelajaran</li>
  </ol>
</div>

<div class="row mb-3">

  <?php if ($this->session->userdata('sess_role') == 'admin') { ?>
    <div class="col-lg-6">
      <div class="card mb-4">
        <div class="card-header bg-gradient-primary py-3 d-flex flex-row align-items-center justify-content-between">
          <h6 class="m-0 text-white"><i class="fa fa-briefcase"></i> Tambah Mata Pelajaran Baru</h6>
        </div>
        <div class="p-3">
          <form method="POST" action="<?= base_url('akademik/pelajaran_create'); ?>">
            <div class="form-group">
              <label for="mata_pelajaran">Mata Pelajaran <span class="text-danger">*</span></label>
              <input type="text" class="form-control" id="mata_pelajaran" name="mata_pelajaran" required="required">
            </div>
            <button type="submit" class="btn btn-primary btn-icon-split">
              <span class="icon text-white-50">
                <i class="fas fa-save"></i>
              </span>
              <span class="text">Simpan</span>
            </button>
          </form>
        </div>
      </div>
    </div>
  <?php } ?>
  <div class="col-lg-6">
    <div class="card mb-4">
      <div class="card-header bg-gradient-primary py-3 d-flex flex-row align-items-center justify-content-between">
        <h6 class="m-0 text-white"><i class="fa fa-briefcase"></i> Daftar Mata Pelajaran</h6>
      </div>
      <div class="table-responsive p-3">
        <table class="table align-items-center table-flush table-hover">
          <thead class="thead-light">
            <tr>
              <th>No.</th>
              <th>Mata Pelajaran</th>
              <?php if ($this->session->userdata('sess_role') == 'admin') { ?>
                <th>Aksi</th>
              <?php } ?>
            </tr>
          </thead>

          <tbody>
            <?php $i = 1;
            foreach ($pelajaran as $data) {
            ?>
              <tr>
                <td><?= $i++; ?></td>
                <td><?= $data->mata_pelajaran; ?></td>
                <?php if ($this->session->userdata('sess_role') == 'admin') { ?>
                  <td>
                    <a href="javascript:void(0)" onclick="edit_data('<?= $data->id; ?>')" class="btn btn-primary" title="Edit Data"><i class="fa fa-edit"></i></a>
                    <a href="<?= base_url('akademik/delete/' . $data->id . '/pelajaran') ?>" onclick="return confirm('Apakah anda yakin?')" class="btn btn-danger" title="Hapus Data"><i class="fa fa-trash"></i></a>
                  </td>
                <?php } ?>
              </tr>
            <?php } ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>

<!-- Modal Edit -->
<div class="modal fade" id="edit_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content">

      <div class="modal-header bg-gradient-primary text-white">
        <h5 class="modal-title" id="exampleModalScrollableTitle">Ubah Tahun Ajaran</h5>
        <button type="button" class="close  text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="POST" action="<?= base_url('akademik/pelajaran_update'); ?>">
          <input type="hidden" id="ed_id" name="ed_id">
          <div class="form-group">
            <label for="ed_mata_pelajaran">Mata Pelajaran <span class="text-danger">*</span></label>
            <input type="text" class="form-control" id="ed_mata_pelajaran" name="ed_mata_pelajaran" placeholder="Masukkan Masukkan Mata pelajaran" required="required">
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Tutup</button>
        <button type="submit" class="btn btn-primary btn-icon-split">
          <span class="icon text-white-50">
            <i class="fas fa-save"></i>
          </span>
          <span class="text">Simpan Perubahan</span>
        </button>
        </form>
      </div>
    </div>
  </div>
</div>

<script>
  var base_url = "<?php echo base_url() ?>";
  $(document).ready(function() {});

  function edit_data(id) {
    $.ajax({
      url: base_url + "akademik/detail/" + id + "/pelajaran",
      type: "GET",
      success: function(result) {
        var obj = JSON.parse(result);

        $('#ed_id').val(obj['id']);
        $('#ed_mata_pelajaran').val(obj['mata_pelajaran']);
        $('#edit_modal').modal('show');
      }
    });
  }
</script>
<?php $this->load->view('templates/footer') ?>