<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <link href="<?=base_url('assets/gambar/logo.png');?>" rel="icon">
  <title>Login - SINILAI</title>
  <link href="<?=base_url('assets/vendor/fontawesome-free/css/all.min.css');?>" rel="stylesheet" type="text/css">
  <link href="<?=base_url('assets/vendor/bootstrap/css/bootstrap.min.css');?>" rel="stylesheet" type="text/css">
  <link href="<?=base_url('assets/css/ruang-admin.min.css');?>" rel="stylesheet">

</head>

<body class="bg-gradient-login">
  <!-- Login Content -->
  <div class="container-login">
    <div class="row justify-content-center">
      <div class="col-xl-10 col-lg-12 col-md-9">
        <div class="card shadow-sm my-5">
          <div class="card-body p-0">
            <div class="row">
              <div class="col-lg-12">
                <div class="card-header bg-gradient-primary text-white text-center">
                  <h5 class="modal-title" id="exampleModalScrollableTitle">
                  Selamat Datang
                </h5>

                </div>
                <div class="card-body">

                  <h6 class="text-center mb-3">Silahkan masukkan username dan password anda :)</h6>
                  <div class="alert alert-danger alert-dismissible" role="alert" id="alert_login" style="display: none;">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">×</span>
                    </button>
                  </div>
                  <form class="user" method="POST" action="<?=base_url('auth/auth_login');?>">
                    <div class="form-group">
                      <input type="text" class="form-control" name="username" placeholder="Masukkan Username">
                    </div>
                    <div class="form-group">
                      <input type="password" class="form-control" name="password" placeholder="Password">
                    </div>
                    <div class="form-group">
                      <div class="custom-control custom-checkbox small" style="line-height: 1.5rem;">
                        <input type="checkbox" class="custom-control-input" id="customCheck">
                        <label class="custom-control-label" for="customCheck">Remember
                        Me</label>
                      </div>
                    </div>
                    <div class="form-group">
                      <button type="submit" class="btn btn-primary btn-block">Masuk</button>
                    </div>
                  </form>
                  <hr>
                  <div class="text-center">
                    <a class="font-weight-bold small" href="<?=base_url('.');?>"><i class="fa fa-arrow-left"></i> Kembali ke menu utama</a>
                  </div>
                  <div class="text-center">
                  </div>
                </div>

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Login Content -->
  
  <script src="<?=base_url('assets/vendor/jquery/jquery.min.js');?>"></script>
  <script src="<?=base_url('assets/vendor/bootstrap/js/bootstrap.bundle.min.js');?>"></script>
  <script src="<?=base_url('assets/vendor/jquery-easing/jquery.easing.min.js');?>"></script>
  <script>
    var flashdata = "<?php echo $this->session->flashdata('errors') ?>";
    $(document).ready(function() {
      if (flashdata != '') {
        $('#alert_login').removeAttr('style');
        $('#alert_login').append(flashdata);
      }
    });
  </script>
</body>

</html>