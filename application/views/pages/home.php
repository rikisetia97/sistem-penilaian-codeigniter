<?php $this->load->view('templates/header') ?>
<?php $this->load->view('templates/sidebar') ?>
<?php $this->load->view('templates/navbar') ?>

<?php if ($this->session->userdata('sess_role') == 'siswa') { ?>
  <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Hai <?php echo $this->session->userdata('sess_nama');?>....</h1>
  </div>
  <div class="row mb-3">
    <div class="col-xl-12 col-lg-12">
      <div class="card mb-4">
        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
          <h6 class="m-0 font-weight-bold text-primary">Grafik Perkembangan Nilai</h6>
        </div>
        <div class="card-body">
          <div class="chart-area">
            <canvas id="myAreaChart" width="634" height="320" class="chartjs-render-monitor" style="display: block; width: 317px; height: 160px;">

            </canvas>
          </div>
        </div>
      </div>
    </div>
  </div>
<?php }else{ ?> 
  <div class="row mb-3">

    <!-- Data Siswa -->
    <div class="col-xl-4 col-md-6 mb-4">
      <div class="card h-100">
        <div class="card-body">
          <div class="row no-gutters align-items-center">
            <div class="col mr-2">
              <div class="text-xs font-weight-bold text-uppercase mb-1">Siswa</div>
              <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800"><?php echo count($this->db->get_where('users', ['role' => 'siswa'])->result()); ?></div>
              <div class="mt-2 mb-0 text-muted text-xs">
                <span>( Jumlah siswa keseluruhan )</span>
              </div>
            </div>
            <div class="col-auto">
              <i class="fas fa-user-graduate fa-2x text-primary"></i>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Mata Pelajaran -->
    <div class="col-xl-4 col-md-6 mb-4">
      <div class="card h-100">
        <div class="card-body">
          <div class="row no-gutters align-items-center">
            <div class="col mr-2">
              <div class="text-xs font-weight-bold text-uppercase mb-1">Guru</div>
              <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800"><?php echo count($this->db->get_where('users', ['role' => 'guru'])->result()); ?></div>
              <div class="mt-2 mb-0 text-muted text-xs">
                <span>( Jumlah guru keseluruhan )</span>
              </div>
            </div>
            <div class="col-auto">
              <i class="fas fa-user-graduate fa-2x text-primary"></i>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Kelas -->
    <div class="col-xl-4 col-md-6 mb-4">
      <div class="card h-100">
        <div class="card-body">
          <div class="row no-gutters align-items-center">
            <div class="col mr-2">
              <div class="text-xs font-weight-bold text-uppercase mb-1">Administrator</div>
              <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800"><?php echo count($this->db->get_where('users', ['role' => 'admin'])->result()); ?></div>
              <div class="mt-2 mb-0 text-muted text-xs">
                <span>( Jumlah administrator keseluruhan )</span>
              </div>
            </div>
            <div class="col-auto">
              <i class="fas fa-user-graduate fa-2x text-primary"></i>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Data Siswa -->
    <div class="col-xl-6 col-md-6 mb-6">
      <div class="card h-100">
        <div class="card-body">
          <div class="row no-gutters align-items-center">
            <div class="col mr-2">
              <div class="text-xs font-weight-bold text-uppercase mb-1">Mata Pelajaran</div>
              <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800"><?php echo count($this->db->get('pelajaran')->result()); ?></div>
              <div class="mt-2 mb-0 text-muted text-xs">
                <span>( Jumlah mata pelajaran keseluruhan )</span>
              </div>
            </div>
            <div class="col-auto">
              <i class="fas fa-swatchbook fa-2x text-primary"></i>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Mata Pelajaran -->
    <div class="col-xl-6 col-md-6 mb-6">
      <div class="card h-100">
        <div class="card-body">
          <div class="row no-gutters align-items-center">
            <div class="col mr-2">
              <div class="text-xs font-weight-bold text-uppercase mb-1">Jurusan</div>
              <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800"><?php echo count($this->db->get('jurusan')->result()); ?></div>
              <div class="mt-2 mb-0 text-muted text-xs">
                <span>( Jumlah jurusan keseluruhan )</span>
              </div>
            </div>
            <div class="col-auto">
              <i class="fas fa-swatchbook fa-2x text-primary"></i>
            </div>
          </div>
        </div>
      </div>
    </div>
 

  </div>

<?php } ?>
<script>
  var base_url = "<?php echo base_url(); ?>";
</script>

  <script src="<?=base_url('assets/js/demo/chart-area-demo.js');?>"></script>

<?php $this->load->view('templates/footer') ?>
