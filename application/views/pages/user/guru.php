<?php $this->load->view('templates/header') ?>
<?php $this->load->view('templates/sidebar') ?>
<?php $this->load->view('templates/navbar') ?>

<!-- BreadCumb-->
<div class="d-sm-flex align-items-center justify-content-between">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?=base_url('.');?>"><i class="fa fa-home"></i> Dashboard</a></li>
    <li class="breadcrumb-item">Users</li>
    <li class="breadcrumb-item active">Guru</li>
  </ol>
  <div align="right">
   <!--  <a href="#" class="btn btn-success btn-icon-split mb-2" onclick="alert('Sek sebentar...')">
      <span class="icon text-white-50">
        <i class="fas fa-print"></i>
      </span>
      <span class="text">Cetak</span>
    </a> -->
    <?php if ($this->session->userdata('sess_role')=='admin') { ?> 
      <a href="#" class="btn btn-primary btn-icon-split mb-2" data-toggle="modal" data-target="#exampleModalScrollable" id="#modalScroll">
        <span class="icon text-white-50">
          <i class="fas fa-plus"></i>
        </span>
        <span class="text">Tambah Guru</span>
      </a>
    <?php } ?>
  </div>
</div>

<div class="row mb-3">

  <div class="col-lg-12">
    <div class="card mb-4">
      <div class="card-header py-3 d-flex flex-row bg-gradient-primary  align-items-center justify-content-between">
        <h6 class="m-0 text-white"><i class="fa fa-users"></i> Daftar Guru</h6>
      </div>
      <div class="table-responsive p-3">
        <table class="table align-items-center table-flush table-hover" id="dataTableHover">
          <thead class="thead-light">
            <tr>
              <th>No.</th>
              <th>Nama</th>
              <th>Mata Pelajaran</th>
              <th>Email</th>
              <th>No. HP</th>
              <th>Alamat</th>
              <th>Jenis Kelamin</th>
              <th>Terdaftar tanggal</th>
              <?php if ($this->session->userdata('sess_role') == 'admin') { ?> 
                <th>Aksi</th>
              <?php } ?>
            </tr>
          </thead>
          <tfoot>
            <tr>
              <th>No.</th>
              <th>Nama</th>
              <th>Mata Pelajaran</th>
              <th>Email</th>
              <th>No. HP</th>
              <th>Alamat</th>
              <th>Jenis Kelamin</th>
              <th>Terdaftar tanggal</th>
              <?php if ($this->session->userdata('sess_role') == 'admin') { ?> 
                <th>Aksi</th>
              <?php } ?>
            </tr>
          </tfoot>
          <tbody>
            <?php $i=1; foreach ($users as $data) {
              if ($data->email == '') { $data->email = '-'; }
              if ($data->no_telpon == '') { $data->no_telpon = '-'; }
              ?>
              <tr>
                <td><?=$i++;?></td>
                <td><?=$data->nama;?></td>
                <td><?=@$this->db->get_where('pelajaran', ['id' => $data->id_pelajaran])->row()->mata_pelajaran ? $this->db->get_where('pelajaran', ['id' => $data->id_pelajaran])->row()->mata_pelajaran: '-';?></td>
                <td><?=$data->email;?></td>
                <td><?=$data->no_telpon;?></td>
                <td><?=$data->alamat;?></td>
                <td><?=$data->jenis_kelamin;?></td>
                <td><?=date('d M Y H:i:s', strtotime($data->created_at));?></td>
                <?php if ($this->session->userdata('sess_role') == 'admin') { ?> 
                  <td>
                    <a href="javascript:void(0)" onclick="edit_data('<?=$data->id;?>')" class="btn btn-primary" title="Edit Data"><i class="fa fa-edit"></i></a>
                    <a href="<?=base_url('users/delete/'.$data->id)?>" onclick="return confirm('Apakah anda yakin?')" class="btn btn-danger" title="Hapus Data"><i class="fa fa-trash"></i></a>
                  </td>
                <?php } ?>
              </tr>
            <?php } ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>

</div>
<!-- Modal Scrollable -->
<div class="modal fade" id="exampleModalScrollable" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content">

      <div class="modal-header bg-gradient-primary text-white">
        <h5 class="modal-title" id="exampleModalScrollableTitle">Tambah Guru Baru</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="alert alert-primary alert-dismissible" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
          <h6><i class="fas fa-bell"></i><b> Pemberitahuan!</b></h6>
          <span style="font-size: 11px;"><i class="fa fa-arrow-right"></i> <b>Username</b> : tergenerate berdasarkan Nama, cth : <b>Andi Saputra</b>, maka usernamenya adalah <b>andi_saputra</b> <br>
            <i class="fa fa-arrow-right"></i> <b>Password</b> : tergenerate berdasarkan tanggal lahir, cth : <b>1997/12/08</b> (yy/mm/dd), maka passwordnya adalah <b>08121997</b> (dd/mm/yy)</span>
          </div>
          <form method="POST" action="<?=base_url('users/create/guru');?>">
            <div class="form-group">
              <label for="nama">Nama <span class="text-danger">*</span></label>
              <input type="text" class="form-control" id="nama" name="nama" placeholder="Masukkan Nama" required="required">
            </div>
            <div class="form-group" id="simple-date3">
              <label for="tgl_lahir">Tanggal Lahir <span class="text-danger">*</span></label>
              <div class="input-group date">
                <div class="input-group-prepend">
                  <span class="input-group-text"><i class="fas fa-calendar"></i></span>
                </div>
                <input type="text" class="form-control" id="tgl_lahir" name="tgl_lahir" placeholder="Masukkan Tanggal Lahir" required="required">
              </div>
            </div>
            <div class="form-group">
              <label for="alamat">Alamat <span class="text-danger">*</span></label>
              <textarea class="form-control" placeholder="Masukkan Alamat Rumah" id="alamat" name="alamat" required="required"></textarea>
            </div>
            <div class="form-group">
              <label for="id_pelajaran">Pilih Mapel <span class="text-danger">*</span></label>
              <select class="form-control" id="id_pelajaran" name="id_pelajaran">
                <?php
                $data = $this->db->get('pelajaran')->result();
                foreach ($data as $item) { ?> 
                  <option value="<?=$item->id;?>"><?=$item->mata_pelajaran;?></option>
                <?php } ?>
              </select>
            </div>
            <div class="form-group">
              <label for="jenis_kelamin">Jenis Kelamin <span class="text-danger">*</span></label>
              <select class="form-control" id="jenis_kelamin" name="jenis_kelamin">
                <option value="P">Perempuan</option>
                <option value="L">Laki - Laki</option>
              </select>
            </div>
            <div class="form-group">
              <label for="email">Email</label>
              <input type="email" class="form-control" id="email" name="email" placeholder="Masukkan Alamat Email">
            </div>
            <div class="form-group">
              <label for="no_telpon">Nomor Telpon</label>
              <input type="text" class="form-control" id="no_telpon" name="no_telpon" placeholder="Masukkan Nomor Telpon">
            </div>

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Tutup</button>
            <button type="submit" class="btn btn-primary btn-icon-split">
              <span class="icon text-white-50">
                <i class="fas fa-save"></i>
              </span>
              <span class="text">Simpan</span>
            </button>
          </form>
        </div>

      </div>
    </div>
  </div>

  <!-- Modal Edit -->
  <div class="modal fade" id="edit_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable" role="document">
      <div class="modal-content">

        <div class="modal-header bg-gradient-primary text-white">
          <h5 class="modal-title" id="exampleModalScrollableTitle">Ubah Data Guru</h5>
          <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form method="POST" action="<?=base_url('users/update');?>">
            <input type="hidden" id="ed_id" name="ed_id">
            <div class="form-group">
              <label for="ed_nama">Nama <span class="text-danger">*</span></label>
              <input type="text" class="form-control" id="ed_nama" name="ed_nama" placeholder="Masukkan Nama" required="required">
            </div>
            <div class="form-group">
             <label for="id_pelajaran">Pilih Mapel <span class="text-danger">*</span></label>
             <select class="form-control" id="ed_id_pelajaran" name="ed_id_pelajaran">
              <?php
              $data = $this->db->get('pelajaran')->result();
              foreach ($data as $item) { ?> 
                <option value="<?=$item->id;?>"><?=$item->mata_pelajaran;?></option>
              <?php } ?>
            </select>
          </div>
          <div class="form-group" id="simple-date3">
            <label for="ed_tgl_lahir">Tanggal Lahir <span class="text-danger">*</span></label>
            <div class="input-group date">
              <div class="input-group-prepend">
                <span class="input-group-text"><i class="fas fa-calendar"></i></span>
              </div>
              <input type="text" class="form-control" id="ed_tgl_lahir" name="ed_tgl_lahir" placeholder="Masukkan Tanggal Lahir" required="required">
            </div>
          </div>
          <div class="form-group">
            <label for="ed_alamat">Alamat <span class="text-danger">*</span></label>
            <textarea class="form-control" placeholder="Masukkan Alamat Rumah" id="ed_alamat" name="ed_alamat" required="required"></textarea>
          </div>
          <div class="form-group">
            <label for="ed_jenis_kelamin">Jenis Kelamin <span class="text-danger">*</span></label>
            <select class="form-control" id="ed_jenis_kelamin" name="ed_jenis_kelamin">
              <option value="P">Perempuan</option>
              <option value="L">Laki - Laki</option>
            </select>
          </div>

          <div class="form-group">
            <label for="ed_email">Email</label>
            <input type="email" class="form-control" id="ed_email" name="ed_email" placeholder="Masukkan Alamat Email">
          </div>
          <div class="form-group">
            <label for="ed_no_telpon">Nomor Telpon</label>
            <input type="text" class="form-control" id="ed_no_telpon" name="ed_no_telpon" placeholder="Masukkan Nomor Telpon">
          </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Tutup</button>
          <button type="submit" class="btn btn-primary btn-icon-split">
            <span class="icon text-white-50">
              <i class="fas fa-save"></i>
            </span>
            <span class="text">Simpan Perubahan</span>
          </button>
        </form>
      </div>

    </div>
  </div>
</div>
<script>
  var base_url = "<?php echo base_url() ?>";
  $(document).ready(function () {
    $('#dataTableHover').DataTable();
    $('#simple-date3 .input-group.date').datepicker({
      startView: 2,
      format: 'yyyy/mm/dd',        
      autoclose: true,     
      todayHighlight: true,   
      todayBtn: 'linked',
    });
  });


  function edit_data(id) {
    $.ajax({
      url: base_url+"users/get_user/"+id, 
      type: "GET",
      success: function(result){
        var obj = JSON.parse(result);

        //convert from yyy/mm/dd to mm/dd/yyy

        $('#ed_id').val(obj['id']);
        $('#ed_nama').val(obj['nama']);
        $('#ed_tgl_lahir').val(obj['tgl_lahir']);
        $('#ed_alamat').val(obj['alamat']);
        $('#ed_jenis_kelamin').val(obj['jenis_kelamin']);
        $('#ed_no_induk').val(obj['no_induk']);
        $('#ed_id_kelas').val(obj['id_kelas']);
        $('#ed_email').val(obj['email']);
        $('#ed_no_telpon').val(obj['no_telpon']);
        $('#edit_modal').modal('show');

      }
    });
  }

</script>
<?php $this->load->view('templates/footer') ?>
